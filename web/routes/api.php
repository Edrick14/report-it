<?php

use Illuminate\Http\Request;
use App\Events\SendPosition;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/total-civilians', 'API\CivilianController@totalCivilians');

Route::get('/total-officers', 'API\OfficerController@totalOfficers');

Route::get('/total-registration-requests', 'API\RegistrationRequestController@totalRegistrationRequests');

Route::get('/total-today-crime-reports', 'API\CrimeController@todayCrimeReports');

Route::get('/incidents', 'API\CrimeController@incidents');

Route::post('/crime-reports', 'API\CrimeController@crimeReports');

Route::get('/crime-barangays', 'API\CrimeController@crimeBarangays');



Route::post('/map', function(Request $request) {

	$lat = $request->lat;
	$lng = $request->long;

	$location = ['lat' => $lat, 'long' => $lng];

	event(new SendPosition($location));

	return response()->json(['status' => 'success', 'data' => $location]);


});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('admin.reports.includes.report-data');
});

Route::post('admin/report-generate', 'ReportController@generateReport')->name('admin.generate.report');

Route::post('file-upload', 'UserController@fileUploadPost')->name('fileUploadPost');


Route::post('incident-upload', 'UserController@uploadIncident')->name('incident.upload');

Route::post('upload-valid-id', 'UserController@uploadValidID')->name('upload.valid.id');

Route::post('upload-profile-picture', 'UserController@uploadProfilePicture')->name('upload.profile.picture');

Route::post('remove-profile-picture', 'ProfileController@removeProfilePhoto')->name('remove.profile.picture');



Route::get('/', function(){

	return view('welcome');

});

Auth::routes();

Route::get('/admin/dashboard', 'DashboardController@index')->name('admin.dashboard');

Route::get('/admin/statistics/{encrypted_id}', 'DashboardController@statistics')->name('admin.statistics');


Route::get('/admin/profile', 'ProfileController@index')->name('admin.profile');

Route::post('admin/profile-save', 'ProfileController@updateProfle')->name('update.profile');


Route::post('/crime-report-to-officer', 'ReportController@setReportToOfficer');

Route::post('/fetch-geofence', 'ReportController@fetchGeofence');


/*Administrator*/

/*Login*/
Route::get('/admin/login', 'Auth\LoginController@adminLogin')->name('admin.login');

Route::get('/officer/login', 'Auth\LoginController@officerLogin')->name('officer.login');

Route::get('/civilian/login', 'Auth\LoginController@civilianLogin')->name('civilian.login');

Route::get('/civilian/register', 'Auth\RegisterController@civilianRegister')->name('civilian.register');


/*Reports*/
Route::get('/admin/reports', 'ReportController@index')->name('admin.reports');

Route::post('/admin/reports-server-side', 'ReportController@serverSideReports')->name('admin.reports-server-side');	

Route::post('/admin/accepted-reports-server-side', 'ReportController@serverSideAcceptedReports')->name('admin.accepted-reports-server-side');	

Route::post('/admin/pending-reports-server-side', 'ReportController@serverSidePendingReports')->name('admin.pending-reports-server-side');	

Route::post('/admin/report-add', 'ReportController@addReport')->name('admin.report.add');

Route::post('/admin/report-delete', 'ReportController@deleteReport')->name('admin.report.delete');

Route::get('/admin/crime-report-load', 'ReportController@eventLoadTotalCrimeReport')->name('load.total.crime.reports');

Route::get('/admin/report-details/{encrypted_id}', 'ReportController@showReport')->name('admin.report.show');

Route::post('/admin/report-load-add', 'ReportController@loadAddReport')->name('admin.report.load.add');

Route::post('/admin/report-load-edit', 'ReportController@loadEditReport')->name('admin.report.load.edit');

Route::get('admin/report-route/{encrypted_id}', 'ReportController@incidentRoute')->name('admin.report.route');


/*Users*/

Route::get('/admin/manage-users/civilians', 'CivilianController@index')->name('admin.manage-users.civilians');

Route::get('/admin/manage-users/civilians/profile/{encrypted_id}', 'CivilianController@profile')->name('admin.manage-users.civilians.profile');

Route::post('/admin/manage-users/civilians-server-side', 'CivilianController@serverSideCivilians')->name('admin.manage-users.civilian-server-side');

Route::get('/admin/manage-users/civilians-load', 'CivilianController@eventLoadTotalCivilian')->name('load.total.civilians');


/*Requests*/

Route::get('/admin/manage-users/registration-requests', 'RegistrationRequestController@index')->name('admin.manage-users.registration-requests');

Route::post('/admin/manage-users/registration-requests-server-side', 'RegistrationRequestController@serverSideRequests')->name('admin.manage-users.registration-requests-server-side');

Route::post('/admin/manage-users/registration-request-accept', 'RegistrationRequestController@acceptCivilian')->name('admin.manage-users.registration-request-accept');

Route::post('/admin/manage-users/registration-request-decline', 'RegistrationRequestController@declineCivilian')->name('admin.manage-users.registration-request-decline');

Route::get('/admin/manage-users/registration-request-load', 'RegistrationRequestController@eventLoadTotalRegistrationRequest')->name('load.total.requests');



/*Police Officers*/

Route::get('/admin/manage-users/police-officers', 'OfficerController@index')->name('admin.manage-users.police-officers');

Route::get('/admin/manage-users/police-officers/profile/{encrypted_id}', 'OfficerController@profile')->name('admin.manage-users.police-officers.profile');

Route::post('/admin/manage-users/police-officers-server-side', 'OfficerController@serverSideOfficers')->name('admin.manage-users.police-officers-server-side');

Route::post('/admin/manage-users/officer-reports-server-side', 'OfficerController@serverSideOfficerReports')->name('admin.officer-reports-server-side');


Route::post('/admin/manage-users/police-officers-register', 'OfficerController@registerOfficer')->name('admin.manage-users.police-officers-register');

Route::get('/admin/manage-users/police-officers-load', 'OfficerController@eventLoadTotalOfficer')->name('load.total.officers');





/*Administrators*/


Route::get('/admin/manage-users/admins', 'AdminController@index')->name('admin.manage-users.admins');

Route::post('/admin/manage-users/admins-server-side', 'AdminController@serverSideAdmins')->name('admin.manage-users.admins-server-side');

Route::post('/admin/manage-users/admins-register', 'AdminController@registerAdmin')->name('admin.manage-users.admins-register');



/*Settings*/
Route::get('/admin/settings', 'SettingsController@index')->name('admin.settings');

// Barangay
Route::post('/admin/settings/barangay-server-side', 'BarangayController@serverSideBarangay')->name('admin.settings.barangay-server-side');

Route::post('/admin/settings//barangay-add', 'BarangayController@addBarangay')->name('admin.settings.barangay-add');

Route::post('/admin/settings//barangay-edit', 'BarangayController@editBarangay')->name('admin.settings.barangay-edit');

Route::post('/admin/settings//barangay-delete', 'BarangayController@deleteBarangay')->name('admin.settings.barangay-delete');

// Incident
Route::post('/admin/settings/incident-server-side', 'CrimeController@serverSideIncidents')->name('admin.settings.incident-server-side');

Route::post('/admin/settings/incident-add', 'CrimeController@addCrime')->name('admin.settings.incident-add');

Route::post('/admin/settings/incident-edit', 'CrimeController@editCrime')->name('admin.settings.incident-edit');


Route::post('/admin/settings/incident-delete', 'CrimeController@deleteCrime')->name('admin.settings.incident-delete');




/*Civilian*/
/*Dashboard*/
Route::get('/civilian/dashboard', 'DashboardController@civilianIndex')->name('civilian.dashboard');

/*Reports*/
Route::get('/civilian/reports', 'ReportController@civilianIndex')->name('civilian.reports');


Route::get('/civilian/reports-create', 'ReportController@civilianCreateReportIndex')->name('civilian.reports.create');

Route::post('/civilian/report-add', 'ReportController@civilianAddReport')->name('civilian.report.add');



Route::get('/civilian/profile', 'ProfileController@civilianIndex')->name('civilian.profile');





/*Officer*/

// Dashboard
Route::get('/officer/dashboard', 'DashboardController@officerIndex')->name('officer.dashboard');

// Statistics
Route::get('/officer/statistics/{encrypted_id}', 'DashboardController@officerStatistics')->name('officer.statistics');

// Reports
Route::get('/officer/reports', 'ReportController@officerIndex')->name('officer.reports');

Route::get('/officer/reports/pending', 'ReportController@officerPendingIndex')->name('officer.pending.reports');

Route::get('/officer/reports/accepted', 'ReportController@officerAcceptedIndex')->name('officer.accepted.reports');



//  Profile
Route::get('/officer/profile', 'ProfileController@officerIndex')->name('officer.profile');

// Maps
Route::get('/officer/crime-map', 'MapController@officerIndex')->name('officer.map');


Route::get('/officer/reports-list', 'ReportController@crimeReports')->name('officer.reports.list');


Route::get('/officer/pending-reports-list', 'ReportController@pendingOfficerCrimeReports')->name('officer.pending.reports.list');


Route::get('/officer/report/{encrypted_id}', 'ReportController@officerReportDetails')->name('officer.report.details');


Route::post('/officer/report/accept', 'ReportController@officerAcceptReport')->name('officer.report.accept');


Route::post('/officer/report-add', 'ReportController@officerAddReport')->name('officer.report.add');


Route::get('officer/report-route/{encrypted_id}', 'ReportController@incidentRoute')->name('officer.report.route');


// Route::get('/event/track-latest-crime', 'ReportController@eventTrackLatestCrime')->name('event.track.latest.crime');



Route::get('/migrate-fresh', function() {
    $migrage = Artisan::call('migrate:fresh');  
    return '<h1>Migrate Fresh</h1>';
});


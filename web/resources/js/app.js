
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

import PrettyCheckbox from 'pretty-checkbox-vue';
import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

Vue.use(datePicker);
Vue.use(PrettyCheckbox);

Vue.component('generate-report', require('./components/GenerateReport.vue').default);

Vue.component('google-maps', require('./components/GoogleMaps.vue').default);
Vue.component('geo-fence', require('./components/Geofence.vue').default);
Vue.component('civilian-geo-fence', require('./components/CivilianGeofence.vue').default);
Vue.component('google-maps-search', require('./components/GoogleMapsSearch.vue').default);
Vue.component('google-maps-search-edit', require('./components/GoogleMapsSearchEdit.vue').default);
Vue.component('dashboard', require('./components/Dashboard.vue').default);

Vue.component('route', require('./components/Route.vue').default);


Vue.component('total-civilians', require('./components/TotalCivilians.vue').default);
Vue.component('total-officers', require('./components/TotalOfficers.vue').default);
Vue.component('total-registration-requests', require('./components/TotalRegistrationRequests.vue').default);


Vue.prototype.$userId = document.querySelector("meta[name='user-id']").getAttribute('content');
Vue.prototype.$incidentLat = document.querySelector("meta[name='incident-lat']").getAttribute('content');
Vue.prototype.$incidentLng = document.querySelector("meta[name='incident-lng']").getAttribute('content');


const app = new Vue({
    el: '#app',
});

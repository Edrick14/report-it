import Vue from 'vue'
import VueRouter from 'vue-router'

//imports
import Dashboard from '../components/admin/Dashboard.vue';
import Report from '../components/admin/Report.vue';
import Profile from '../components/admin/Profile.vue';
import RegisterOfficer from '../components/admin/management/RegisterOfficer.vue';
import Users from '../components/admin/management/Users.vue';
import RegistrationRequests from '../components/admin/management/RegistrationRequests.vue';
 
Vue.use(VueRouter)

//routes
const  routes = [
  { path: '/admin/dashboard', component: Dashboard, name: 'admin.dashboard' },
  { path: '/admin/report', component: Report, name: 'admin.report' },
  { path: '/admin/profile', component: Profile, name: 'admin.profile' },
  { path: '/admin/management/users', component: Users, name: 'admin.users'  },
  { path: '/admin/management/registration-requests', component: RegistrationRequests, name: 'admin.registration.requests'  },
  { path: '/admin/management/register-officer', component: RegisterOfficer, name: 'admin.register.officer'  }
 ]

export default new VueRouter({
    mode: 'history',
    routes
})
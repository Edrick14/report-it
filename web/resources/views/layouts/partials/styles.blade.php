  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

 <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}">
  <!-- iCheck -->
  {{-- <link rel="stylesheet" href="{{ asset('/adminlte/plugins/iCheck/flat/blue.css') }}"> --}}

  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/iCheck/all.css') }}">
 

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Datatables -->
  <!-- <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"> -->

  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap4.css') }}">

  <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/datatables.net/Responsive-2.2.2/css/responsive.dataTables.min.css') }}">

  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('/toastr/build/toastr.min.css') }}">


  <link rel="stylesheet" href="{{ asset('/paginationjs/dist/pagination.css') }}">

  <link rel="stylesheet" href="{{ asset('/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/croppie/croppie.css') }}">


  <!-- <link rel="stylesheet" href="{{ asset('/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css') }}"> -->

<style type="text/css">
 
 .card-comments .card-comment img, .img-sm, .user-block.user-block-sm img {
    width: 25px!important;
    height: 25px!important;
}

.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px;
    user-select: none;
    -webkit-user-select: none;
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 31px;
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: 6px;
    right: 1px;
    width: 20px;
}

</style>
 <aside class="main-sidebar @yield('sidebar-variants') elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link @yield('brand-variants')">
         <img src="{{ asset('/img/light.png') }}" alt="Report IT Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Report IT</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img  src="{{ isset(auth()->user()->profile_picture) ? url('').auth()->user()->profile_picture : (auth()->user()->gender == 'male' ? asset('/img/no-photo-male.png') : asset('/img/no-photo-female.png')) }}" class="img-circle elevation-2 profile-photo" alt="User Image">
        </div>
        <div class="info">
          @auth
            <a href="#"  class="d-block"><div id="user_full_name">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</div> </a>
          @endauth
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          @auth

            @if(Auth::user()->role == 'admin')  
              <li class="nav-item">
                <a href="{{ route('admin.dashboard') }}"   class="nav-link @yield('d-active')">
                   <i class="nav-icon fa fa-dashboard"></i>
                  <p>
                    Dashboard
                   </p>
                </a>
              </li>

                <?php $r =  new App\DataAccessLayer\ReportsDAO() ?>
              <li class="nav-item">
                <a href="{{ route('admin.reports') }}" class="nav-link @yield('rprts-active')">
                    <i class="nav-icon fa fa-file"></i>
                  <p>
                    Crime Reports <span class="badge badge-danger navbar-badge"> {{ $r->collection()->get()->count() }}</span>
                   </p>
                </a>
              </li>

       
              <li class="nav-item has-treeview @yield('menu-open')">
                <a href="#" class="nav-link @yield('c-active') @yield('rq-active') @yield('r-active')">
                  <i class="nav-icon fa fa-cog"></i>
                  <p>
                    Manage Users
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="{{ route('admin.manage-users.civilians') }}" class="nav-link @yield('c-active')">
                      <i class="fa fa-users nav-icon"></i>
   
                      <p>Civilians</p>  <span class="badge badge-danger navbar-badge"><total-civilians></total-civilians></span>   
                    </a>
                  </li> 
                  <li class="nav-item">
                    <a href="{{ route('admin.manage-users.registration-requests') }}" class="nav-link @yield('rq-active')">
                      <i class="fa fa-users nav-icon "></i>
                      <p>Registration Requests</p> <span class="badge badge-warning navbar-badge"><total-registration-requests></total-registration-requests></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.manage-users.police-officers') }}" class="nav-link @yield('r-active')">
                      <i class="fa fa-users  nav-icon"></i>
                      <p>Police Officers</p>  <span class="badge badge-primary navbar-badge"><total-officers></total-officers></span>
                    <a/>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('admin.manage-users.admins') }}" class="nav-link @yield('a-active')">
                      <i class="fa fa-users  nav-icon"></i>
                      <p>Administrators</p>  
                    <a/>
                  </li>
                </ul>
              </li>

            
     

               <li class="nav-item">
                <a href="{{ route('admin.settings') }}" class="nav-link @yield('set-active')">
                   <i class="nav-icon fa fa-cogs"></i>
                  <p>
                    Settings
                   </p>
                </a>
              </li>

                <li class="nav-item">
                <a href="{{ route('admin.profile') }}" class="nav-link @yield('p-active')">
                   <i class="nav-icon fa fa-user"></i>
                  <p>
                    Profile
                   </p>
                </a>
              </li>

            @elseif(Auth::user()->role == 'civilian')

              <li class="nav-item">
                <a href="{{ route('civilian.dashboard') }}"   class="nav-link @yield('d-active')">
                   <i class="nav-icon fa fa-dashboard"></i>
                  <p>
                    Dashboard
                   </p>
                </a>
              </li>



            @if(Auth::user()->is_user_valid)
               <li class="nav-item">
                <a href="{{ route('civilian.reports.create') }}" class="nav-link @yield('crprts-active')">
                    <i class="nav-icon fa fa-pencil"></i>
                  <p>

                    Report a Crime 
                   </p>
                </a>
              </li>
              @endif


             <li class="nav-item">
                <a href="{{ route('civilian.reports') }}" class="nav-link @yield('rprts-active')">
                    <i class="nav-icon fa fa-file"></i>
                  <p>
                    Crime Reports
                   </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('civilian.profile') }}" class="nav-link @yield('p-active')">
                   <i class="nav-icon fa fa-user"></i>
                  <p>
                    Profile
                   </p>
                </a>
              </li>

            <!-- <civilian-geo-fence></civilian-geo-fence> -->
             

            @elseif(Auth::user()->role == 'officer')

               <li class="nav-item">
                <a href="{{ route('officer.dashboard') }}"   class="nav-link @yield('d-active')">
                   <i class="nav-icon fa fa-dashboard"></i>
                  <p>
                    Crime Dashboard
                   </p>
                </a>
              </li>

                <li class="nav-item">
                <a href="{{ route('officer.map') }}"   class="nav-link @yield('cm-active')">
                   <i class="nav-icon fa fa-map"></i>
                  <p>
                    Crime Map
                   </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('officer.accepted.reports') }}" class="nav-link @yield('rprts-active')">
                    <i class="nav-icon fa fa-file"></i>
                  <p>
                    Crime Reports
                   </p>
                </a>
              </li>

                <?php $r =  new App\DataAccessLayer\ReportsDAO() ?>
              
               <li class="nav-item">
                <a href="{{ route('officer.pending.reports') }}" class="nav-link @yield('prprts-active')">
                    <i class="nav-icon fa fa-file"></i>
                  <p>
                    Pending Reports  <span class="badge badge-danger navbar-badge"> {{ $r->pendingOfficerCollection()->get()->count() }}</span> 
                   </p>
                </a>
              </li>
   

              <li class="nav-item">
                <a href="{{ route('officer.profile') }}" class="nav-link @yield('p-active')">
                   <i class="nav-icon fa fa-user"></i>
                  <p>
                    Profile
                   </p>
                </a>
              </li>

            <geo-fence></geo-fence> 


            @endif

          @endauth

          
         
          <li class="nav-item">
            <a  href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class="nav-link">
               <i class="nav-icon fa fa-power-off"></i>
              <p>
                {{ __('Logout') }}
               </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>



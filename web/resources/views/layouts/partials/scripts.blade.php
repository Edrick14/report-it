

<!-- jQuery -->
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/iCheck/icheck.min.js') }}"></script>

 
<!-- Input Mask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
  
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>



<!-- AdminLTE App -->
<script src="{{ asset('/adminlte/dist/js/adminlte.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>


<!-- Datatables -->
<!-- <script src="{{ asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
 -->
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap4.js') }}"></script>

<script src="{{ asset('/adminlte/bower_components/datatables.net/Responsive-2.2.2/js/dataTables.responsive.min.js') }}"></script>

<script src="{{ asset('/adminlte/bower_components/moment/min/moment.min.js') }}"></script>



<script src="{{ asset('/toastr/build/toastr.min.js') }}"></script>

<script src="{{ asset('/sweetalert/docs/assets/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/chartjs-old/Chart.min.js') }}"></script>

 <!-- Pagination JS -->
<script src="{{ asset('/paginationjs/dist/pagination.js') }}"></script>

<script src="{{ asset('/js/admin/loads.js') }}"></script>

<script src="{{ asset('/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>

<script src="{{ asset('/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js') }}"></script>
 
<script src="{{ asset('/croppie/croppie.js') }}"></script>


<script>

  
     $(function () {
          //Initialize Select2 Elements
         $('.select2').select2()


          //Input Mask
         $('[data-mask]').inputmask();

       
         $('#map-date-time-picker-to').datetimepicker();

         $('#map-date-time-picker-from').datetimepicker();

         // $('[data-toggle="tooltip"]').tooltip()
          $('#emergency').change(function() {
            $('#emergency_status').html($(this).prop('checked') ? 'Emergency' : 'Non-emergency')
          })


    });

	var server_side_user_url = "{{ route('admin.manage-users.civilian-server-side') }}";
	
	var server_side_request_url = "{{ route('admin.manage-users.registration-requests-server-side') }}";

	var server_side_officers_url = "{{ route('admin.manage-users.police-officers-server-side') }}";

  var server_side_admins_url = "{{ route('admin.manage-users.admins-server-side') }}";

	var server_side_reports_url = "{{ route('admin.reports-server-side') }}";

	var server_side_accepted_reports_url = "{{ route('admin.accepted-reports-server-side') }}";

	var server_side_pending_reports_url = "{{ route('admin.pending-reports-server-side') }}";

   var server_side_barangay_url = "{{ route('admin.settings.barangay-server-side') }}";

   var server_side_incident_url = "{{ route('admin.settings.incident-server-side') }}";


   var server_side_officer_reports_url = "{{ route('admin.officer-reports-server-side') }}";


   var officer_reports_list_url = "{{ route('officer.reports.list') }}";

   var officer_pending_reports_list_url  = "{{ route('officer.pending.reports.list') }}";

   var load_total_officers_url = "{{ route('load.total.officers') }}";

   var load_total_requests_url = "{{ route('load.total.requests') }}";

   var load_total_civilians_url = "{{ route('load.total.civilians') }}";

   var load_total_crime_reports_url = "{{ route('load.total.crime.reports') }}";

   
   var admin_report_load_edit = "{{ route('admin.report.load.edit') }}";

   var admin_report_load_add = "{{ route('admin.report.load.add') }}";

   var user_profile_picture_url = "{{ isset(auth()->user()->profile_picture) ? url('').auth()->user()->profile_picture : (auth()->user()->gender == 'male' ? asset('/img/no-photo-male.png') : asset('/img/no-photo-female.png')) }}";


   var upload_profile_picture_url = "{{ route('upload.profile.picture') }}";

   var remove_profile_picture_url = "{{ route('remove.profile.picture') }}";


</script>




<script src="{{ asset('/js/admin/civilians.js') }}"></script>

<script src="{{ asset('/js/admin/reports.js') }}"></script>

<script src="{{ asset('/js/admin/requests.js') }}"></script>

<script src="{{ asset('/js/admin/officers.js') }}"></script>

<script src="{{ asset('/js/admin/admins.js') }}"></script>

<script src="{{ asset('/js/admin/barangay.js') }}"></script>

<script src="{{ asset('/js/admin/incident.js') }}"></script>

<script src="{{ asset('/js/admin/settings.js') }}"></script>

@yield('scripts')

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyXzfrXuKeardoOze30WW9wq6SzDT4_T4&libraries=geometry,places" async defer></script> -->

<script src="{{ asset('/js/app.js') }}"></script>



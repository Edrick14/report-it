 <aside class="main-sidebar @yield('sidebar-variants') elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
         <img src="{{ asset('/img/logo.png') }}" alt="Report IT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Report IT</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('/img/user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}"   class="nav-link @yield('d-active')">
               <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
               </p>
            </a>
          </li>
          


          <li class="nav-item">
            <a href="{{ route('admin.reports') }}" class="nav-link @yield('rprts-active')">
                <i class="nav-icon fa fa-file"></i>
              <p>
                Crime Reports
               </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.profile') }}" class="nav-link @yield('p-active')">
               <i class="nav-icon fa fa-user"></i>
              <p>
                Profile
               </p>
            </a>
          </li>
 
          <li class="nav-item has-treeview @yield('menu-open')">
            <a href="#" class="nav-link @yield('c-active') @yield('rq-active') @yield('r-active')">
              <i class="nav-icon fa fa-cogs"></i>
              <p>
                Manage Users
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.manage-users.civilians') }}" class="nav-link @yield('c-active')">
                  <i class="fa fa-users nav-icon"></i>
                  <p>Civilians</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.manage-users.registration-requests') }}" class="nav-link @yield('rq-active')">
                  <i class="fa fa-users nav-icon "></i>
                  <p>Registration Requests</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.manage-users.police-officers') }}" class="nav-link @yield('r-active')">
                  <i class="fa fa-users  nav-icon"></i>
                  <p>Police Officers</p>
                <a/>
              </li>
            </ul>
          </li>
         

          <li class="nav-item">
            <a  href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class="nav-link">
               <i class="nav-icon fa fa-power-off"></i>
              <p>
                {{ __('Logout') }}
               </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


@auth

	@if(Auth::user()->role == 'admin')

		@section('navbar-variants','bg-info navbar-dark')
		@section('sidebar-variants','sidebar-dark-info')
		@section('brandlogo-variants','bg-info')
		@section('brand-variants','')


	@elseif(Auth::user()->role == 'officer')

		@section('navbar-variants','bg-primary navbar-dark')
		@section('sidebar-variants','sidebar-light-primary')
		@section('brandlogo-variants','bg-primary')
		@section('brand-variants','bg-primary')


	@elseif(Auth::user()->role == 'civilian')

		@section('navbar-variants','bg-danger navbar-dark')
		@section('sidebar-variants','sidebar-light-danger')
		@section('brandlogo-variants','bg-danger')
		@section('brand-variants','bg-danger')


	@else

		@section('navbar-variants','bg-white navbar-light')
		@section('sidebar-variants','sidebar-dark-primary')

	@endif

@endauth


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Incident Directions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="user-id" content="{{ Auth::user()->id }}">
  <meta name="incident-lat" content="{{ $report->lat }}">
  <meta name="incident-lng" content="{{ $report->lng }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

  @include('layouts.partials.styles')
 
</head>
<body>

   @if($report->status == 'pending')
   <button style="position: absolute;
    right: 0;
    bottom: 0;
    z-index: 1;"  id="accept-crime-report-btn" data-encrypted-id="{{ Crypt::encrypt($report->id) }}" data-action="{{ route('officer.report.accept') }}"  type="button" class="btn btn-block btn-danger btn-md"> <i class="fa fa-map-marker"></i> Accept Report</button>
    @endif

 

  <div id="app"> 
    <route></route>
  </div>
   
 
  @include('layouts.partials.scripts')

</body>
</html>
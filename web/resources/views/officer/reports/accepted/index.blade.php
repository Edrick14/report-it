@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Reports')

@section('rprts-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection




@section('content')

  <div class="content-wrapper">
	

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reports</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    


      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
             <div class="card" id="reports-card">
            <div class="card-header">
              <h3 class="card-title">Crime Incident Reports</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" id="add-crime-report-btn"><i class="fa fa-plus"></i>
                 <button type="button" class="btn btn-tool" id="refresh-areports-btn" data-toggle="tooltip" data-placement="top" title="Refresh" ><i class="fa fa-refresh"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <div id="add-report-wrapper"  style="display: none">
                  @include('officer.reports.accepted.includes.add-crime-report-form')
              </div>

              <div class="row">
                 <div class="col-lg-12">

                    <div class="row"  id="reports-container"></div>

                    <div id="reports-pagination-container"></div>
                   
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>




    </section>
    <!-- /.content -->
</div>


@endsection


  
@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Reports')

@section('rprts-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection




@section('content')

  <div class="content-wrapper">
	

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reports</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    


      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
            <div class="card" id="reports-card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" id="nav-officer-accepted" href="#officer-accepted" data-toggle="tab">Crime Reports</a></li>
                  <li class="nav-item"><a class="nav-link" id="nav-officer-pending" href="#officer-pending" data-toggle="tab">Pending Reports </a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                
                  @include('officer.reports.includes.tab-accepted-reports')
                  @include('officer.reports.includes.tab-pending-reports')

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>




    </section>
    <!-- /.content -->
</div>


@endsection

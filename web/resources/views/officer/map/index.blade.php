@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Profile')

@section('cm-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Map</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
   {{--   <div class="checkbox">
      <label>
        <input type="checkbox" data-on="Emergency" data-off="Emergency" data-onstyle="danger" data-toggle="toggle">
        Option one is enabled
      </label>
    </div> --}}

        <google-maps></google-maps>

 
    </section>
    <!-- /.content -->
</div>

@endsection

@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Dashboard')

@section('d-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Statistics</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
    
          <!-- /.col (LEFT) -->
          <div class="col-md-12">
           
            <!-- BAR CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ $barangay->name }}</h3>

                <div class="card-tools">
                
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barangay-statistics" style="height: 400px; width: 340px;" width="892" height="446"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
</div>

<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
  $(function(){


    var areaChartData = <?php echo json_encode($data) ?>

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barangay-statistics').get(0).getContext('2d');
    var barChart                         = new Chart(barChartCanvas);
    var barChartData                     = areaChartData;

    // alert(JSON.stringify(barChartData.datasets[0].fillColor));

    barChartData.datasets[0].fillColor   = '#00a65a';
    barChartData.datasets[0].strokeColor = '#00a65a';
    barChartData.datasets[0].pointColor  = '#00a65a';
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: false,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : false,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 1,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 12,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,

      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);

  });
</script>

@endsection

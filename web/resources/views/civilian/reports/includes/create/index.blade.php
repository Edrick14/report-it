@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Reports')

@section('crprts-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <link rel="stylesheet" href="{{ asset('/jquery-file-uploader/css/jquery.fileupload.css') }}">

  <div class="content-wrapper">
	

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    

      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
             <div class="card" id="reports-card">
            <div class="card-header">
              <h3 class="card-title">New Crime Incident</h3>
              <div class="card-tools">
        
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">


              <form method="POST" action="{{ route('civilian.report.add') }}" data-civilian="yes" id="frm-crime-report">
                
                <div class="row">
                  <div class="col-md-6">
                  
                    <div class="form-group" style="display: none">
                      <label for="name">First Name*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->first_name }}" id="first_name">
                      <span role="alert" class="invalid-feedback" id="first_name_feedback"></span>

                    </div>

                    <div class="form-group" style="display: none">
                      <label for="name">Last Name*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->last_name }}" id="last_name">
                      <span role="alert" class="invalid-feedback" id="last_name_feedback"></span>

                    </div>

                    <div class="form-group" style="display: none">
                      <label for="name">Middle Name</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->middle_name }}" id="middle_name">
                      <span role="alert" class="invalid-feedback" id="middle_name_feedback"></span>

                    </div>
               
                   <div class="form-group" style="display: none">
                    <label  for="mobile">Mobile Number*</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="mobile" value="{{ auth()->user()->mobile }}" data-inputmask='"mask": "+639999999999"' data-mask>
                      <span role="alert" class="invalid-feedback" id="mobile_feedback"></span>

                    </div>
                  </div>


                   <div class="form-group">
                    <label>Reporter Type</label>
                    <select id="reporter_type" class="form-control" style="width: 100%; height: 40px">
                      <option value="victim" selected="selected">Victim</option>
                      <option value="witness">Witness</option>
                    </select>
                  </div>

                    <div class="form-group" style="display: none">
                      <label for="name">Reporter Home Address*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->home_address }}" id="reporter_home_address">
                      <span role="alert" class="invalid-feedback" id="reporter_home_address_feedback"></span>

                    </div>

                  <div class="form-group" style="display: none">
                      <label for="name">Reporter Barangay*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->barangay }}" id="reporter_barangay">
                      <span role="alert" class="invalid-feedback" id="reporter_barangay_feedback"></span>

                    </div>

                 
                     <div class="form-group" style="display: none">
                      <label for="name">Reporter Town/City*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->city }}" id="reporter_city">
                      <span role="alert" class="invalid-feedback" id="reporter_city_feedback"></span>

                    </div>

                 <div class="form-group" style="display: none">
                      <label for="name">Reporter Province*</label>
                      <input type="text" class="form-control" value="{{ auth()->user()->province }}" id="reporter_province">
                      <span role="alert" class="invalid-feedback" id="reporter_province_feedback"></span>

                    </div>

                   
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                  
                     <div class="form-group">
                    <label>Type of Incident</label>
                    <select id="incident" class="form-control select2" style="width: 100%; height: 10px">
                        @foreach($dal->CrimeDAO()->collection()->get() as $model)
                           <option value="{{ $model->id }}">{{ $model->name }}</option>
                        @endforeach
                    </select>
                  </div>


                    <div class="form-group">
                      <label>Incident Barangay</label>
                      <select id="barangay" class="form-control select2" style="width: 100%; height: 10px">

                        @foreach($dal->BarangayDAO()->collection()->get() as $model)
                           <option value="{{ $model->id }}">{{ $model->name }}</option>
                        @endforeach

                      </select>
                    </div>

            <div class="form-group">
          
               <label>Picture of Incident</label>
              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success btn-sm btn-block fileinput-button">
                       <span>Upload</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('incident.upload') }}" id="incident_image" class="form-control" type="file" name="incident_image">
                     
                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 1.5rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="percent"></span>
                    </div>
                  </div>
                </div>
              </div>

               <input type="hidden"  name="image_file"   class="form-control is-invalid" id="image_file">
               <span role="alert" class="invalid-feedback" id="image_file_feedback" ></span>

            </div>
                    
                    <google-maps-search></google-maps-search>
            
                    <div class="form-group" style="margin-top: 10px">
                      <label for="description">Description</label>
                      <textarea type="text" class="form-control" id="description"></textarea> 
                    </div>

                  </div>
                  <!-- /.col -->
                </div>


              </form>
        

            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            
           <!--    <input type="checkbox" id="emergency" data-on="Enabled" data-off="Disabled" data-onstyle="danger" data-size="small" data-toggle="toggle">

              <label id="emergency_status" style="margin-top: 4px;margin-left: 5px;">Non-emergency</label>     -->

            <button type="submit" form="frm-crime-report" id="add-report-sbmt" class="btn btn-danger btn-sm float-right" style="width: 85px">Save</button>
          </div>

          </div>
          <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>






    </section>
    <!-- /.content -->
</div>

@endsection


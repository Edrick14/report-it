@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Profile')

@section('p-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <link rel="stylesheet" href="{{ asset('/jquery-file-uploader/css/jquery.fileupload.css') }}">


  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
     

      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-danger card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle profile-photo" src="{{ isset(auth()->user()->profile_picture) ? url('').auth()->user()->profile_picture : (auth()->user()->gender == 'male' ? asset('/img/no-photo-male.png') : asset('/img/no-photo-female.png')) }}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ auth()->user()->name }}</h3>

                <p class="text-muted text-center">Civilian | Member since {{ \Carbon\Carbon::parse(auth()->user()->created_at)->toFormattedDateString() }}</p>

 
             <!--    <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                   </li>
                  <li class="list-group-item">
                   </li>
                 
                </ul> -->

                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
           <!--  <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div> -->
              <!-- /.card-header -->
          <!--     <div class="card-body">
                <strong><i class="fa fa-phone mr-1"></i> Mobile Number</strong>

                <p class="text-muted">
                 </p>

                <hr>
 
              </div>
 -->              <!-- /.card-body -->
            <!-- </div> -->
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card" id="civilian-profile-card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" id="nav-profile" href="#profile" data-toggle="tab">Profile</a></li>
                 </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                
                  @include('civilian.profile.includes.tab-profile')
              

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

    </section>
    <!-- /.content -->
</div>

@endsection


@section('scripts')

<script src="{{ asset('/js/admin/profile.js') }}"></script>

@endsection

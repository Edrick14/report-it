@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Dashboard')

@section('d-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <dashboard></dashboard>
        <google-maps></google-maps>

        <div class="card">
          <div class="card-header">
             <h3 class="card-title">Crime Incidents/Barangay</h3>
          </div>
          <div class="card-body">
              <div class="row">
              @foreach($dal->BarangayDAO()->collection()->get() as $model)
                <div class="col-lg-3 col-6">
                  <!-- small card -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>{{ $model->crimeReports->where('status', 'accepted')->count() }}</h3>

                      <p>{{ $model->name }}</p>
                    </div>
                    <div class="icon">
                      <!-- <i class="fa fa-shopping-cart"></i> -->
                    </div>
                    <a target="_blank" href="{{ route('admin.statistics', Crypt::encrypt($model->id)) }}" class="small-box-footer">
                      View Stats <i class="fa fa-arrow-circle-right"></i>
                    </a>
                  </div>
                </div>
                <!-- ./col -->
              @endforeach
            </div>
          </div>
        </div>
      
    </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
     
    </section>
    <!-- /.content -->
</div>

@endsection

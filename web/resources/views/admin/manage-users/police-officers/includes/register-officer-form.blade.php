<h3 class="card-title">Register Officer</h3>

<div class="card-body">
  
    <form method="POST" action="{{ route('admin.manage-users.police-officers-register') }}" id="frm-register-officer">
    
      <div class="row">
        <div class="col-md-6">
        
          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name">
            <span role="alert" class="invalid-feedback" id="first_name_feedback"></span>
          </div>

          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name">
            <span role="alert" class="invalid-feedback" id="last_name_feedback"></span>
          </div>

          <div class="form-group">
            <label for="middle_name">Middle Name</label>
            <input type="text" class="form-control" id="middle_name">
            <span role="alert" class="invalid-feedback" id="middle_name_feedback"></span>
          </div>
     
         <div class="form-group">
          <label  for="mobile">Mobile Number</label>
          <div class="input-group">
            <input type="text" class="form-control" id="mobile" data-inputmask='"mask": "+639999999999"' data-mask>
            <span role="alert" class="invalid-feedback" id="mobile_feedback"></span>
            
          </div>

        </div>


        <div class="form-group">
          <label>Gender</label>
          <select id="gender" class="form-control select2" style="width: 100%; height: 10px">
            <option value="male" selected="selected">Male</option>
            <option value="female">Female</option>
          </select>
        </div>

        <div class="form-group">
          <label for="destination">Destination</label>
          <input type="text" class="form-control" id="destination">
          <span role="alert" class="invalid-feedback" id="destination_feedback"></span>
        </div>
   
       <div class="form-group">
          <label for="position">Position</label>
          <input type="text" class="form-control" id="position">
          <span role="alert" class="invalid-feedback" id="position_feedback"></span>
        </div>  

     

         
        </div>
        <!-- /.col -->
        <div class="col-md-6">
        
           <div class="form-group">
            <label for="home_address">Home Address</label>
            <input type="text" class="form-control" id="home_address">
            <span role="alert" class="invalid-feedback" id="home_address_feedback"></span>

          </div>
     
          <div class="form-group">
            <label for="barangay">Barangay</label>
            <input type="text" class="form-control" id="barangay">
            <span role="alert" class="invalid-feedback" id="barangay_feedback"></span>

          </div>
     
   

          <div class="form-group">
            <label for="city">City</label>
            <input type="text" class="form-control" id="city"> 
            <span role="alert" class="invalid-feedback" id="city_feedback"></span>

          </div>


         <div class="form-group">
            <label for="province">Province</label>
            <input type="text" class="form-control" id="province"> 
            <span role="alert" class="invalid-feedback" id="province_feedback"></span>

          </div>
     
           <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" class="form-control" id="email"> 
            <span role="alert" class="invalid-feedback" id="email_feedback"></span>

          </div>
     
        
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password"> 
            <span role="alert" class="invalid-feedback" id="password_feedback"></span>

          </div>

          <div class="form-group">
            <label for="confirm_password">Confirm Password</label>
            <input type="password" class="form-control" id="confirm_password"> 
          </div>

     
        

        </div>
        <!-- /.col -->
      </div>

    </form>

</div>

<div class="card-footer">
  <button type="submit" form="frm-register-officer" id="register-officer-sbmt" class="btn btn-primary btn-sm" style="width: 85px">Register</button>
  <button type="button" class="btn btn-default btn-sm float-right"  id="cancel2-register-btn" style="width: 85px">Cancel</button>
</div>
<hr>            

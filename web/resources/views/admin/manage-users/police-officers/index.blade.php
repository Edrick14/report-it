@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Police Officers')

@section('menu-open','menu-open')

@section('r-active', 'active')

@section('sidebar')
  @include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Police Officers</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    
       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Officers List</h3>
              <div class="card-tools">
                   <button type="button" class="btn btn-primary btn-sm" id="register-officer-btn" >Register</button> 
                <button type="button" class="btn btn-default btn-sm" id="cancel-register-btn" style="display: none">Cancel</button>
                  <button type="button" class="btn btn-tool" id="refresh-officers-btn" data-toggle="tooltip" data-placement="top" title="Refresh" ><i class="fa fa-refresh"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            
                <div id="register-officer-wrapper"  style="display: none">
                    @include('admin.manage-users.police-officers.includes.register-officer-form')
                 </div>
             <!--   <div class="table-responsive">
              </div> -->

              <table  id="officers-tbl" class="table table-striped table-hover display responsive no-wrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="th-sm" style="color: #595f65 !important;">Name
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Mobile
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Email
                      </th>
                         <th class="th-sm" style="color: #595f65 !important;">Home Address
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Barangay
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Action
                       </th>
                    
                    </tr>
                  </thead>
  
                </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
</div>

@endsection

@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Registration Requests')

@section('menu-open','menu-open')

@section('rq-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Registration Requests</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
     
       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">User Requests List</h3>
              <div class="card-tools">
              {{--   <span class="badge badge-warning"><span id="total_requests">{{ $total_requests }}</span>  User Request(s)</span> --}}
                 <button type="button" class="btn btn-tool" id="refresh-requests-btn" data-toggle="tooltip" data-placement="top" title="Refresh" ><i class="fa fa-refresh"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
             

             <!--   <div class="table-responsive">
              </div> -->

              <table  id="requests-tbl" class="table table-striped table-hover display responsive no-wrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="th-sm" style="color: #595f65 !important;">Date Requested
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Name
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Mobile
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Email
                      </th>
                         <th class="th-sm" style="color: #595f65 !important;">Home Address
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Barangay
                       </th>
                        <th class="th-sm" style="color: #595f65 !important;">1st ID
                       </th>
                        <th class="th-sm" style="color: #595f65 !important;">2nd ID
                       </th>
                      <th class="th-sm" style="color: #595f65 !important;">Action
                       </th>
                    
                    </tr>
                  </thead>
                
                <!--   <tfoot>
                    <tr>
                      <th>Name</i>
                      </th>
                      <th>Address</i>
                      </th>
                      <th>Mobile</i>
                      </th>
                      <th>Email</i>
                      </th>
                      <th>Action</i>
                      </th>
                  
                    </tr>
                  </tfoot> -->
                </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
     
    </section>
    <!-- /.content -->
</div>

@endsection

<div class="tab-pane" id="pending">
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
    
            <!-- /.card-header -->
            <div class="card-body">
             

             <!--   <div class="table-responsive">
              </div> -->

              <table  id="pending-reports-tbl"  data-encrypted-id="{{ Crypt::encrypt($user->id) }}" class="table table-striped table-hover" width="100%">
                  <thead>
                      <tr>
                      <th class="th-sm">Date
                       </th>
                      <th class="th-sm">Time
                       </th>
                      <th class="th-sm">Name
                       </th>
                      <th class="th-sm">Type of Crime
                       </th>
                       <th class="th-sm">Barangay
                       </th>
                     <!--     <th class="th-sm">Location
                       </th>
                         <th class="th-sm">Approved By
                       </th> -->
                        <th class="th-sm">Status
                       </th>
                        <th class="th-sm">Action
                       </th>
                    
                    </tr>
                  </thead>
                
                <!--   <tfoot>
                    <tr>
                      <th>Name</i>
                      </th>
                      <th>Address</i>
                      </th>
                      <th>Mobile</i>
                      </th>
                      <th>Email</i>
                      </th>
                      <th>Action</i>
                      </th>
                  
                    </tr>
                  </tfoot> -->
                </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</div>
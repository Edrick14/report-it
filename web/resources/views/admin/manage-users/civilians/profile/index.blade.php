  @extends('layouts.master')

@include('layouts.theme')

@section('title', $user->name )

@section('menu-open','menu-open')

@section('c-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection

@section('content')

 <!--  
<style type="text/css">
  
#civilian-profile-card .active {

  background-color: #dc3545 !important;

}



#civilian-profile-card a:hover {

  color: #dc3545 !important;

}

#civilian-profile-card .active a:hover {

  color: #fff !important;

}

</style> -->

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
  

      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-danger card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('/img/user.png') }}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ $user->name }}</h3>

                <p class="text-muted text-center">Civilian</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Accepted Reports</b> <a class="float-right">{{ $user->crimeReport->where('status', 'accepted')->count() }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Pending Reports</b> <a class="float-right">{{ $user->crimeReport->where('status', 'pending')->count() }}</a>
                  </li>
                 
                </ul>

                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fa fa-phone mr-1"></i> Mobile Number</strong>

                <p class="text-muted">
                  {{ isset($user->mobile) ? $user->mobile : '' }}
                </p>

                <hr>

               <strong><i class="fa fa-envelope mr-1"></i> Email Address</strong>
                <p class="text-muted"> {{ $user->email }}</p>

                <hr>

                <strong><i class="fa fa-map-marker mr-1"></i> Home Address</strong> 
                  <p class="text-muted"> 
                    {{ isset($user->home_address) ? $user->home_address : '' }}
                  </p>

                <hr>

                <strong><i class="fa fa-map-marker mr-1"></i> Barangay</strong> 
                  <p class="text-muted"> 
                    {{ isset($user->barangay) ? $user->barangay : '' }}
                  </p>

                <hr>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card" id="civilian-profile-card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" id="nav-accepted" href="#accepted" data-toggle="tab">Accepted Reports</a></li>
                  <li class="nav-item"><a class="nav-link" id="nav-pending" href="#pending" data-toggle="tab">Pending Reports</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                
                  @include('admin.manage-users.civilians.profile.includes.tab-accepted-reports')
                  @include('admin.manage-users.civilians.profile.includes.tab-pending-reports')


                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

    </section>
    <!-- /.content -->
</div>

@endsection

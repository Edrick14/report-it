<div id="modal-generate-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="generate-report" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="generate-report">Generate Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
    
       <form id="frm-generate-report" method="POST" action="{{ route('admin.generate.report') }}">
          <generate-report></generate-report>
       </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="cancel-generate" data-dismiss="modal">Cancel</button>
        <button type="submit" form="frm-generate-report" id="generate-sbmt"  class="btn btn-danger" style="width: 85px">Generate</button>
      
      </div>
    </div>
  </div>
</div>
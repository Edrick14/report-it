<td>  
    <div style="text-align: center;">                                           	                                  
    <!-- 
    	<button type="button" class="btn btn-sm btn-warning edit-report-btn"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}" data-action="" 	data-first-name="{{ $model->first_name }}"
    	data-last-name="{{ $model->last_name }}"
    	data-middle-name="{{ $model->middle_name }}" 
    	data-mobile="{{ $model->mobile }}"
    	data-reporter-type="{{ $model->reporter_type }}"
    	data-home-address="{{ $model->reporter_home_address }}"
    	data-barangay="{{ $model->reporter_barangay }}"
    	data-city="{{ $model->reporter_city }}"
    	data-province="{{ $model->reporter_province }}"
    	data-incident-id="{{ $model->crime_id }}"
    	data-barangay-id="{{ $model->barangay_id }}"
    	data-location="{{ $model->location }}"
    	data-image="{{ $model->image }}"
    	data-lat="{{ $model->lat }}"
    	data-lng="{{ $model->lng }}"
    	data-description="{{ $model->description }}"
    		><i class="fa fa-pencil"></i></button> 
 -->
    	<a target="_blank" href="{{ route('admin.report.show', Crypt::encrypt($model->id)) }}">

    		<button type="button" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button> 

    	</a>

    	<!-- <button type="button" class="btn btn-sm btn-danger remove-report-btn"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}" data-action="{{ route('admin.report.delete') }}" ><i class="fa fa-trash"></i></button>  -->

    </div>
</td>


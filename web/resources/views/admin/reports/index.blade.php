@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Reports')

@section('rprts-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <div class="content-wrapper">
 

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reports</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
     

       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Crime Incident Reports</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-danger btn-sm" id="add-crime-report-btn" >Report</button> 
                <button type="button" class="btn btn-default btn-sm" id="cancel-crime-report-btn" style="display: none">Cancel</button> 
                  <button type="button" data-toggle="modal" data-target="#modal-generate-report" class="btn btn-tool" id="generate-report-btn" data-toggle="tooltip" data-placement="top" title="Generate Report" ><i class="fa fa-files-o"></i>
                </button>
                <button type="button" class="btn btn-tool" id="refresh-crime-report-btn" data-toggle="tooltip" data-placement="top" title="Refresh" ><i class="fa fa-refresh"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
             
             <!--   <div class="table-responsive">
              </div> -->
                
              <div id="add-report-wrapper"  style="display: none">
                  @include('admin.reports.includes.add-crime-report-form')
              </div>

              <div id="edit-report-wrapper"  style="display: none">
                  @include('admin.reports.includes.edit-crime-report-form')
              </div>
              <table  id="reports-tbl" class="table table-striped table-hover display responsive no-wrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                     
                       <th class="th-sm">No.  
                       </th>
                      <th class="th-sm">Date Reported
                       </th>
                      <th class="th-sm">Date Response
                       </th>
                      <th class="th-sm">Name
                       </th>
                      <th class="th-sm">Type of Crime
                       </th>
                       <th class="th-sm">Barangay
                       </th>
                        <!--  <th class="th-sm">Location
                       </th> -->
                         <th class="th-sm">Approved By
                       </th>
                        <th class="th-sm">Status
                       </th>
                        <th class="th-sm">Report Details
                       </th>
                    
                    </tr>
                  </thead>
                         
                </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
       

       @include('admin.reports.modals.modal-generate-report')

    </section>
    <!-- /.content -->
</div>

@endsection

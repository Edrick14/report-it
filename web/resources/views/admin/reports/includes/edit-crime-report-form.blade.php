
<h3 class="card-title">Form</h3>
<div class="card-body">
  
<form method="POST" action="" id="frm-crime-report">
    
      <input type="hidden" id="encrypted_id">

      <div class="row">
        <div class="col-md-6">
        
          <div class="form-group">
            <label for="name">First Name*</label>
            <input type="text" class="form-control" id="edit_first_name">
            <span role="alert" class="invalid-feedback" id="edit_first_name_feedback"></span>

          </div>

          <div class="form-group">
            <label for="name">Last Name*</label>
            <input type="text" class="form-control" id="edit_last_name">
            <span role="alert" class="invalid-feedback" id="edit_last_name_feedback"></span>

          </div>

          <div class="form-group">
            <label for="name">Middle Name</label>
            <input type="text" class="form-control" id="edit_middle_name">
            <span role="alert" class="invalid-feedback" id="edit_middle_name_feedback"></span>

          </div>
     
         <div class="form-group">
          <label  for="mobile">Mobile Number*</label>
          <div class="input-group">
            <input type="text" class="form-control" id="edit_mobile" data-inputmask='"mask": "+639999999999"' data-mask>
            <span role="alert" class="invalid-feedback" id="edit_mobile_feedback"></span>

          </div>
        </div>


         <div class="form-group">
          <label>Reporter Type</label>
          <select id="edit_reporter_type" class="form-control select2" style="width: 100%; height: 10px">
            <option value="victim" selected="selected">Victim</option>
            <option value="witness">Witness</option>
          </select>
        </div>

          <div class="form-group">
            <label for="name">Reporter Home Address*</label>
            <input type="text" class="form-control" id="edit_reporter_home_address">
            <span role="alert" class="invalid-feedback" id="edit_reporter_home_address_feedback"></span>

          </div>

        <div class="form-group">
            <label for="name">Reporter Barangay*</label>
            <input type="text" class="form-control" id="edit_reporter_barangay">
            <span role="alert" class="invalid-feedback" id="edit_reporter_barangay_feedback"></span>

          </div>

       
           <div class="form-group">
            <label for="name">Reporter Town/City*</label>
            <input type="text" class="form-control" id="edit_reporter_city">
            <span role="alert" class="invalid-feedback" id="edit_reporter_city_feedback"></span>

          </div>

       <div class="form-group">
            <label for="name">Reporter Province*</label>
            <input type="text" class="form-control" id="edit_reporter_province">
            <span role="alert" class="invalid-feedback" id="edit_reporter_province_feedback"></span>

          </div>

         
        </div>
        <!-- /.col -->
        <div class="col-md-6">
        
           <div class="form-group">
          <label>Type of Incident</label>
          <select id="edit_incident" class="form-control select2" style="width: 100%; height: 10px">
              @foreach($dal->CrimeDAO()->collection()->get() as $model)
                 <option value="{{ $model->id }}">{{ $model->name }}</option>
              @endforeach
          </select>
        </div>


          <div class="form-group">
            <label>Incident Barangay</label>
            <select id="edit_barangay" class="form-control select2" style="width: 100%; height: 10px">

              @foreach($dal->BarangayDAO()->collection()->get() as $model)
                 <option value="{{ $model->id }}">{{ $model->name }}</option>
              @endforeach

            </select>
          </div>

           <div class="form-group">
          
               <label>Picture of Incident</label>
              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success btn-sm btn-block fileinput-button">
                       <span>Upload</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('incident.upload') }}" id="edit_incident_image" class="form-control" type="file" name="incident_image">
                     
                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 1.5rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="edit_progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="edit_percent"></span>
                    </div>
                  </div>
                </div>
              </div>

               <input type="hidden"  name="image_file"   class="form-control is-invalid" id="edit_image_file">
               <span role="alert" class="invalid-feedback" id="edit_image_file_feedback" ></span>

            </div>
          
          <!-- <google-maps-search-edit></google-maps-search-edit> -->
  
          <div class="form-group" style="margin-top: 10px">
            <label for="description">Description</label>
            <textarea type="text" class="form-control" id="edit_description"></textarea> 
          </div>

        </div>
        <!-- /.col -->
      </div>

    </form>

</div>

<div class="card-footer">
  <button type="submit" form="frm-crime-report" id="add-report-sbmt" class="btn btn-danger btn-sm" style="width: 85px">Save</button>
  <button type="button" class="btn btn-default btn-sm float-right"  id="cancel3-crime-report-btn" style="width: 85px">Cancel</button>
</div>
<hr>            

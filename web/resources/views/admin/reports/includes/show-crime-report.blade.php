@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Report Details')

@section('rprts-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection




@section('content')

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Report Details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
           
          <div class="col-md-6">
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img  src="{{ url('').$crime_report->crime->marker }}" alt="User Image">
                  <span class="username"><a href="#">{{ $crime_report->first_name }} {{ $crime_report->last_name }} ({{ strtoupper($crime_report->reporter_type) }})</a></span>
                  <span class="description">{{ \Carbon\Carbon::parse($crime_report->created_at)->timezone('GMT+8')->toFormattedDateString() }} {{ \Carbon\Carbon::parse($crime_report->created_at)->timezone('GMT+8')->format('g:i A') }}</span>
                     
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
              
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- post text -->


                <!-- Attachment -->
                <div class="attachment-block clearfix">
                      <img src="{{ isset($crime_report->image) ?  url('').$crime_report->image  : asset('/img/no-photo-available.jpg') }}"   style="width: 100%; height: 100%">
                </div>
                <!-- /.attachment-block -->

              </div>
              <!-- /.card-body -->

           

            </div>
            <!-- /.card -->

          <div class="card card-widget">
            <div class="card-header">
              <div class="card-title"> 
                <span class="username">
                  <h5> View Shortest Path</h5>
                </span> 
              </div> 
            </div> 
            <div class="card-body">
                <a href="{{ route('admin.report.route',$encrypted_id) }}">

                  <button type="button" id="incident-route" class="btn btn-block btn-danger btn-md"> <i class="fa fa-map-marker"></i> Click Here...</button>

                </a>
            </div>
          </div>

          </div>
          <!-- /.col -->

           <div class="col-md-6">
            <!-- Box Comment -->
            <div class="card card-widget">
            
               <div class="card-header">
                  <label class="username">Details</label>
                     
                <!-- /.user-block -->
                <div class="card-tools">
              
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->

              <div class="card-body card-comments">

                 <div class="card-comment">     
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                   <div class="comment-text">
                    <span class="username">
                     Reporter Home Address
                    </span><!-- /.username -->
                    {{ $crime_report->reporter_home_address }}
                  </div>
                  <!-- /.comment-text -->
                </div>

               <div class="card-comment">     
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                   <div class="comment-text">
                    <span class="username">
                     Reporter Barangay
                    </span><!-- /.username -->
                    {{ $crime_report->reporter_barangay }}
                  </div>
                  <!-- /.comment-text -->
                </div>

                <div class="card-comment">     
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                   <div class="comment-text">
                    <span class="username">
                     Reporter City
                    </span><!-- /.username -->
                    {{ $crime_report->reporter_city }}
                  </div>
                  <!-- /.comment-text -->
                </div>

              <div class="card-comment">     
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                   <div class="comment-text">
                    <span class="username">
                     Reporter Province
                    </span><!-- /.username -->
                    {{ $crime_report->reporter_province }}
                  </div>
                  <!-- /.comment-text -->
                </div>


                <div class="card-comment">     
                  <img src="{{ asset('/icons/mobile.svg') }}">
                   <div class="comment-text">
                    <span class="username">
                      Reporter Mobile Number
                    </span><!-- /.username -->
                    {{ $crime_report->mobile }}
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
                  <div class="card-comment">
                  <img src="{{ asset('/icons/info.svg') }}">
                  <div class="comment-text">
                    <span class="username">
                      Type Of Incident
                    </span><!-- /.username -->
                    {{ $crime_report->crime->name }}
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
                <div class="card-comment">
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                  <div class="comment-text">
                    <span class="username">
                      Incident Barangay
                    </span><!-- /.username -->
                   {{ $crime_report->barangay->name }}
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
                <div class="card-comment">
                  <img src="{{ asset('/icons/map-marker.svg') }}">
                  <div class="comment-text">
                    <span class="username">
                      Incident Location
                    </span><!-- /.username -->
                   {{ $crime_report->location }}
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
            
                <!-- /.card-comment -->
              </div>
             
       
            
                 
                </div>
              <!-- /.card-footer -->
             

            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

        </div>
        </div>
    </section>
    <!-- /.content -->
</div>


@endsection

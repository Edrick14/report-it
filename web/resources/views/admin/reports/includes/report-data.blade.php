<!DOCTYPE html>
<html>
 <head>
  <title>Report</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container">

      
      <img src="{{ public_path('/img/pnp_logo.png') }}"   style="height: 115px;margin-top: 20px;margin-bottom: 30px;margin-left: 25px;" />
      <label   style="margin-bottom: 70px;margin-left: 20px; margin-right: 20px;  
   
    font-size: 17px;" >
         Summary of Crime Incident Report for {{ \Carbon\Carbon::now()->timezone('GMT+8')->toFormattedDateString() }}
      </label>


   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Nature of Crime</th>
       <th>Approved Crime Incident</th>
       <th>Pending Crime Incident</th>
       <th>Total</th>
      </tr>
     </thead>
     <tbody>


     <?php 

     	$barangays = $dal->BarangayDAO()->collection() ;

     	 if($encrypted_id != '0') {

            $barangays = $barangays->where('id', Crypt::decrypt($encrypted_id));

        }



     ?>

     @foreach($barangays->get() as $barangay)

      <tr>
       <td style="background-color:#E2E2E2"><label>{{ $barangay->name }}</label></td>
       <td style="background-color:#E2E2E2"><label>{{ $dal->ReportsDAO()->collection()->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->where('status','accepted')->count() }}</label></td>
       <td style="background-color:#E2E2E2"><label>{{ $dal->ReportsDAO()->collection()->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->where('status','pending')->count() }}</label></td>
       <td style="background-color:#E2E2E2"><label>{{ $dal->ReportsDAO()->collection()->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->count() }}</label></td>
      </tr>


     

      	@foreach($dal->CrimeDAO()->collection()->get() as $crime)
	      <tr>
	       <td>{{ $crime->name }}</td>
	       <td>{{  $dal->ReportsDAO()->collection()->where('crime_id', $crime->id)->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->where('status','accepted')->count() }}</td>
	       <td>{{  $dal->ReportsDAO()->collection()->where('crime_id', $crime->id)->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->where('status','pending')->count() }}</td>
	       <td>{{  $dal->ReportsDAO()->collection()->where('crime_id', $crime->id)->where('barangay_id', $barangay->id)->where('created_at', 'like', '%' . $date . '%')->count() }}</td>
	      </tr>
	     @endforeach

     @endforeach



  	

   
     </tbody>
    </table>
   </div>
  </div>
 </body>
</html>
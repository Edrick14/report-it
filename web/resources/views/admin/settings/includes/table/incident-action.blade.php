<td>  
    <div style="text-align: center;">                                           	                                  
    
		<button type="button" class="btn btn-sm btn-warning edit-incident-btn" data-marker="{{ $model->marker }}" data-name="{{ $model->name }}" data-file-url="{{ url('').$model->marker }}"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}"  data-action="" ><i class="fa fa-pencil"></i></button> 
    	

    	@if($model->crimeReports->count() <= 0) 
	
			<button type="button" class="btn btn-sm btn-danger remove-incident-btn"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}" data-action="{{ route('admin.settings.incident-delete') }}" ><i class="fa fa-trash"></i></button> 

		@endif

    </div>
</td>


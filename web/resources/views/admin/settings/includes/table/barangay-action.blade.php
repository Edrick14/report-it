<td>  
    <div style="text-align: center;">                                           	                                  
    

    	<button type="button" class="btn btn-sm btn-warning edit-barangay-btn" data-name="{{ $model->name }}"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}" data-action="" ><i class="fa fa-pencil"></i></button> 
    	
    	@if($model->crimeReports->count() <= 0) 
    		
    		<button type="button" class="btn btn-sm btn-danger remove-barangay-btn"  data-encrypted-id="{{ Crypt::encrypt($model->id)}}" data-action="{{ route('admin.settings.barangay-delete') }}" ><i class="fa fa-trash"></i></button> 

    	@endif

    </div>
</td>


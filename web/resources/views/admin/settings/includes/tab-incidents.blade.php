 <div class="tab-pane" id="incident">

 	  <div class="row">

          <div class="col-lg-12">
            <div class="card">
            
             <div class="card-header">
                <h3 class="card-title">Incidents List</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-success btn-sm" id="add-incident-btn" data-toggle="modal" data-target="#modal-add-incident" ><i class="fa fa-plus"></i></button> 
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                    <table  id="incident-tbl" class="table table-striped table-hover" width="100%">
                      <thead>
                          <tr>
                          <th class="th-sm">Date
                           </th>
                           <th class="th-sm">Incident
                           </th>
                           <th class="th-sm" style="width: 20px !important;">Map Marker
                           </th>
                            <th class="th-sm">Action
                           </th>
                        
                        </tr>
                      </thead>
                    
                    <!--   <tfoot>
                        <tr>
                          <th>Name</i>
                          </th>
                          <th>Address</i>
                          </th>
                          <th>Mobile</i>
                          </th>
                          <th>Email</i>
                          </th>
                          <th>Action</i>
                          </th>
                      
                        </tr>
                      </tfoot> -->
                    </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

</div>
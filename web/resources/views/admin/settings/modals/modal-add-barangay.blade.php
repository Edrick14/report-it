<div id="modal-add-barangay" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="add-barangay" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="add-barangay">Add New Barangay</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card-body">
          <form id="frm-add-barangay" method="POST" action="{{ route('admin.settings.barangay-add') }}">
            <div class="form-group">
              <label for="barangay">Barangay</label>
              <input type="text" class="form-control" id="barangay_id">
              <span role="alert" class="invalid-feedback" id="name_feedback"></span>
            </div>  
          </form>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="add-barangay-cancel" data-dismiss="modal">Cancel</button>
        <button type="submit" form="frm-add-barangay" id="add-barangay-sbmt"  class="btn btn-success" style="width: 65px">Add</button>
      
      </div>
    </div>
  </div>
</div>
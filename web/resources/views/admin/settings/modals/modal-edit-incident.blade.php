<div id="modal-edit-incident" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="add-incident" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="add-incident">Add New Incident</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card-body">
          <form id="frm-edit-incident" method="POST" action="{{ route('admin.settings.incident-edit') }}">
            <div class="form-group">
              <label for="incident">Edit Incident</label>
              <input type="text" class="form-control" id="edit_incident_id">
              <span role="alert" class="invalid-feedback" id="edit_incident_feedback"></span>

            </div>  

            <div class="form-group">
            
               <label>Map Marker</label>

              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success  btn-block fileinput-button">
                       <span>Upload</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('fileUploadPost') }}" id="edit_marker" class="form-control" type="file" name="marker">

                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 2rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="edit_progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="edit_percent"></span>
                    </div>
                  </div>
                </div>

              
               <input type="hidden" class="form-control" id="iencrypted_id">


              </div>

              <div class="form-group">
                <input type="hidden"  name=""   class="form-control is-invalid" id="edit_file_location">
                <span role="alert" class="invalid-feedback" id="edit_marker_feedback" ></span>
              </div>
                

              <div class="form-group">

                <img  id="edit_img-marker" style="width: 100%; height: 100%; margin-top: 15px" >
                  
              </div>
              

            </div>  
         
                
            
          </form>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="edit-incident-cancel" data-dismiss="modal">Cancel</button>
        <button type="submit" form="frm-edit-incident" id="edit-incident-sbmt"  class="btn btn-success" style="width: 65px">Save</button>
      
      </div>
    </div>
   
  </div>
</div>
 
<div id="modal-add-incident" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="add-incident" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="add-incident">Add New Incident</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card-body">
          <form id="frm-add-incident" method="POST" action="{{ route('admin.settings.incident-add') }}">
            <div class="form-group">
              <label for="incident">Incident</label>
              <input type="text" class="form-control" id="incident_id">
              <span role="alert" class="invalid-feedback" id="incident_feedback"></span>

            </div>  

            <div class="form-group">
            
               <label>Map Marker</label>

              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success  btn-block fileinput-button">
                       <span>Upload</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('fileUploadPost') }}" id="marker" class="form-control" type="file" name="marker">

                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 2rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="percent"></span>
                    </div>
                  </div>
                </div>

              

              </div>

              <div class="form-group">
                <input type="hidden"  name=""   class="form-control is-invalid" id="file_location">
                <span role="alert" class="invalid-feedback" id="marker_feedback" ></span>
              </div>
                

              <div class="form-group">
               
                <img  id="img-marker" style="display: none; width: 100%; height: 100%; margin-top: 15px" >
                  
              </div>
              

            </div>  
         
                
            
          </form>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="add-incident-cancel" data-dismiss="modal">Cancel</button>
        <button type="submit" form="frm-add-incident" id="add-incident-sbmt"  class="btn btn-success" style="width: 65px">Add</button>
      
      </div>
    </div>
   
  </div>
</div>
 
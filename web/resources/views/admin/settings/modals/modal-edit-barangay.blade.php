<div id="modal-edit-barangay" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-barangay" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit-barangay">Edit Barangay</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card-body">
          <form id="frm-edit-barangay" method="POST" action="{{ route('admin.settings.barangay-edit') }}">
            <div class="form-group">
              <label for="barangay">Barangay</label>
              <input type="text" class="form-control" id="edit_barangay">
              <span role="alert" class="invalid-feedback" id="edit_name_feedback"></span>
            </div>  
          </form>
        </div>

        <input type="hidden" class="form-control" id="encrypted_id">


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="edit-barangay-cancel" data-dismiss="modal">Cancel</button>
        <button type="submit" form="frm-edit-barangay" id="edit-barangay-sbmt"  class="btn btn-success" style="width: 65px">Save</button>
      
      </div>
    </div>
  </div>
</div>
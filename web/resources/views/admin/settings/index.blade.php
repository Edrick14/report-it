@extends('layouts.master')

@include('layouts.theme')

@section('title', 'Settings')

@section('set-active', 'active')

@section('sidebar')
	@include('layouts.partials.sidebar')
@endsection


@section('content')

  <link rel="stylesheet" href="{{ asset('/jquery-file-uploader/css/jquery.fileupload.css') }}">

  <div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Settings</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <!--  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active refresh-settings-tabs" id="nav-accepted" href="#barangay" data-toggle="tab">Barangay</a></li>

                <li class="nav-item"><a class="nav-link refresh-settings-tabs" id="nav-accepted" href="#incident" data-toggle="tab">Incident</a></li>                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                
                  @include('admin.settings.includes.tab-barangay')
                  @include('admin.settings.includes.tab-incidents')

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->

    @include('admin.settings.modals.modal-add-barangay')
    @include('admin.settings.modals.modal-edit-barangay')
    @include('admin.settings.modals.modal-add-incident')
    @include('admin.settings.modals.modal-edit-incident')

</div>

@endsection

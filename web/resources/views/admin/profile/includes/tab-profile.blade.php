<div class="active tab-pane" id="profile">

          
          <div class="row">
            
            <div class="col-md-6">

              <form id="frm-update-profile" action="{{ route('update.profile') }}" method="POST">
              
                 <div class="form-group">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" id="first_name" value="{{ auth()->user()->first_name }}">
                  <span role="alert" class="invalid-feedback" id="first_name_feedback"></span>
                </div>

                <div class="form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" class="form-control" id="last_name"  value="{{ auth()->user()->last_name }}">
                  <span role="alert" class="invalid-feedback" id="last_name_feedback"></span>
                </div>

                <div class="form-group">
                  <label for="middle_name">Middle Name</label>
                  <input type="text" class="form-control" id="middle_name"  value="{{ auth()->user()->middle_name }}">
                  <span role="alert" class="invalid-feedback" id="middle_name_feedback"></span>
                </div>
           
               <div class="form-group">
                <label  for="mobile">Mobile Number</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="mobile" data-inputmask='"mask": "+639999999999"' data-mask  value="{{ auth()->user()->mobile }}">
                  <span role="alert" class="invalid-feedback" id="mobile_feedback"></span>
                  
                </div>

              </div>


              <div class="form-group">
                <label>Gender</label>
                <select id="gender" class="form-control select2" style="width: 100%; height: 10px">
                  <option value="male" {{(auth()->user()->gender == 'male') ?  'selected="selected"' : '' }}>Male</option>
                  <option value="female" {{(auth()->user()->gender == 'female') ?  'selected="selected"' : '' }}>Female</option>
                </select>
              </div>

                <div class="form-group">
                  <label for="home_address">Home Address</label>
                  <input type="text" class="form-control" id="home_address" value="{{ auth()->user()->home_address }}">
                  <span role="alert" class="invalid-feedback" id="home_address_feedback"></span>

                </div>
           
                <div class="form-group">
                  <label for="barangay">Barangay</label>
                  <input type="text" class="form-control" id="barangay" value="{{ auth()->user()->barangay }}">
                  <span role="alert" class="invalid-feedback" id="barangay_feedback"></span>

                </div>
           
         

                <div class="form-group">
                  <label for="city">City</label>
                  <input type="text" class="form-control" id="city" value="{{ auth()->user()->city }}"> 
                  <span role="alert" class="invalid-feedback" id="city_feedback"></span>

                </div>


               <div class="form-group">
                  <label for="province">Province</label>
                  <input type="text" class="form-control" id="province" value="{{ auth()->user()->province }}"> 
                  <span role="alert" class="invalid-feedback" id="province_feedback"></span>

                </div>
           

              </form>
              
            </div>



            <div class="col-md-6">
              
              <div class="upload-canvas-wrapper" style="   width: 300px;height: 300px;margin: 0 auto;">
                  <div id="upload-canvas"></div>
                
              </div>


              <div class="text-center" style="margin-top: 45px">
                    <span class="btn btn-info fileinput-button">
                       <i class="fa fa-upload"></i>
                      <input data-url="{{ route('fileUploadPost') }}" id="upload-profile" class="form-control" type="file">

                  </span>
                  <button class="btn btn-info upload-result" > <i class="fa fa-save"></i></button>
                  <button class="btn btn-info delete-photo" > <i class="fa fa-trash"></i></button>
              </div>

            </div>

          </div>


     

        <div class="card-footer" style="margin-top: 20px">
            <button type="submit" form="frm-update-profile" id="save-profile-sbmt" class="btn btn-success btn-md float-right" style="width: 85px">Save</button>
        </div>    

</div>
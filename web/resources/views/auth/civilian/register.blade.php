
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/iCheck/all.css') }}">

  <link rel="stylesheet" href="{{ asset('/jquery-file-uploader/css/jquery.fileupload.css') }}">


  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


<style type="text/css">
 
 .card-comments .card-comment img, .img-sm, .user-block.user-block-sm img {
    width: 25px!important;
    height: 25px!important;
}


</style>
  <style type="text/css">
  .register-card-body .input-group .input-group-text {
      color: #777 !important;
      background-color: transparent !important;
  }

  </style>

</head>
<body class="hold-transition register-page">

 <div class="register-box">
  <div class="register-logo">
    <a href=""><b>REPORT</b> IT</a>
  </div>
  <!-- /.login-logo -->
  <div class="register-box-body">
    <p class="register-box-msg">Register a new membership</p>

    <form action="{{ route('register') }}" method="POST">
      @csrf
      <div class="input-group mb-3">
          <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus  placeholder="{{ __('First Name') }}">
          <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
          </div>
           @if ($errors->has('first_name'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('first_name') }}</strong>
              </span>
          @endif
    </div>
     <div class="input-group mb-3">
          <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus  placeholder="{{ __('Last Name') }}">
          <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
          </div>
           @if ($errors->has('last_name'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('last_name') }}</strong>
              </span>
          @endif
    </div>

 <div class="input-group mb-3">
          <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}"  autofocus  placeholder="{{ __('Middle Name') }}">
          <div class="input-group-append">
              <span class="fa fa-user input-group-text"></span>
          </div>
           @if ($errors->has('middle_name'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('middle_name') }}</strong>
              </span>
          @endif
    </div>

    <div class="form-group">
    
      <div class="row" >
    
        <div class="col-md-9">
          <label>
            <input type="radio" name="gender" value="male" id="male" checked>
            Male
          </label>

          <label style="margin-left: 15px;">
            <input type="radio" name="gender" value="male" id="female">
            Female
          </label>
        </div>


      </div>
       
    </div>

      <div class="input-group mb-3">
          <input id="mobile" type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required  placeholder="{{ __('Mobile') }}" data-inputmask='"mask": "+639999999999"' data-mask>
          <div class="input-group-append">
              <span class="fa fa-phone input-group-text"></span>
          </div>
           @if ($errors->has('mobile'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('mobile') }}</strong>
              </span>
          @endif
    </div>


    <div class="input-group mb-3">
          <input id="home_address" type="text" class="form-control{{ $errors->has('home_address') ? ' is-invalid' : '' }}" name="home_address" value="{{ old('home_address') }}" required   placeholder="{{ __('Home Address') }}">
          <div class="input-group-append">
              <span class="fa fa-home input-group-text"></span>
          </div>
           @if ($errors->has('home_address'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('home_address') }}</strong>
              </span>
          @endif
    </div>

    <div class="input-group mb-3">
          <input id="barangay" type="text" class="form-control{{ $errors->has('barangay') ? ' is-invalid' : '' }}" name="barangay" value="{{ old('barangay') }}" required   placeholder="{{ __('Barangay') }}">
          <div class="input-group-append">
              <span class="fa fa-home input-group-text"></span>
          </div>
           @if ($errors->has('barangay'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('barangay') }}</strong>
              </span>
          @endif
    </div>

   <div class="input-group mb-3">
          <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required   placeholder="{{ __('Town/City') }}">
          <div class="input-group-append">
              <span class="fa fa-home input-group-text"></span>
          </div>
           @if ($errors->has('city'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('city') }}</strong>
              </span>
          @endif
    </div>

   <div class="input-group mb-3">
          <input id="province" type="text" class="form-control{{ $errors->has('province') ? ' is-invalid' : '' }}" name="province" value="{{ old('province') }}" required   placeholder="{{ __('Province') }}">
          <div class="input-group-append">
              <span class="fa fa-home input-group-text"></span>
          </div>
           @if ($errors->has('province'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('province') }}</strong>
              </span>
          @endif
    </div>  
   
    <div class="input-group mb-3">

          <input type="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}">
          <div class="input-group-append">
              <span class="fa fa-envelope input-group-text"></span>
          </div>
           @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
    </div>
    <div class="input-group mb-3">
      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Password') }}">
      <div class="input-group-append">
          <span class="fa fa-key input-group-text"></span>
      </div>
       @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
   <div class="input-group mb-3">
      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('Confirm Password') }}">
      <div class="input-group-append">
          <span class="fa fa-key input-group-text"></span>
      </div>

    </div>


        <div class="form-group">
          
              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success btn-sm btn-block fileinput-button">
                       <span>Upload Government ID</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('upload.valid.id') }}" id="first_valid_id" class="form-control" type="file" name="first_valid_id">
                     
                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 1.5rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="percent"></span>
                    </div>
                  </div>
                </div>
              </div>

               <input type="hidden"  name="first_valid_id_file" value="{{ old('first_valid_id_file') }}"  class="form-control is-invalid" id="first_valid_id_file">

                @if ($errors->has('first_valid_id_file'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('first_valid_id_file') }}</strong>
                    </span>
                @endif

            </div>

             <div class="form-group">
          
              <div class="row">

                <div class="col-md-3">
                   <span class="btn btn-success btn-sm btn-block fileinput-button">
                       <span>Upload Non-governmnet ID</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input data-url="{{ route('upload.valid.id') }} " id="second_valid_id" class="form-control" type="file" name="second_valid_id">
                     
                  </span>
                </div>

                <div class="col-md-9">
                 <div class="progress" style="height: 1.5rem !important; margin-top: 3px">
                    <div class="progress-bar  progress-bar-striped" id="scndprogress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="" id="scndpercent"></span>
                    </div>
                  </div>
                </div>
              </div>

               <input type="hidden"  name="second_valid_id_file" value="{{ old('second_valid_id_file') }}"  class="form-control is-invalid" id="second_valid_id_file">

                @if ($errors->has('second_valid_id_file'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('second_valid_id_file') }}</strong>
                    </span>
                @endif

            </div>
     
      
    
        <button type="submit" class="btn btn-danger btn-block" style="margin-top: 20px">   {{ __('Register') }}</button>
        <a href="{{ route('civilian.login') }}" class="btn-block">
           <button type="button"  class="btn btn-primary btn-block">   {{ __('I already have a account') }}</button>
        </a>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
 
<!-- Input Mask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>

<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>


<script>




    $('#male').on('ifChecked', function(){

       $(this).val('male');
       $('#female').val('male');


    });

    $('#female').on('ifChecked', function(){

        $(this).val('female');
        $('#male').val('female');



    });

  $(function () {

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    });

    

     $('[data-mask]').inputmask();



    $('#first_valid_id').on('change', function(){

           var fd = new FormData();
            var files = $('#first_valid_id')[0].files[0];

            var imagefile = files.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
          
                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.error('Only jpeg, jpg and png Images type allowed');

              return false;
            }

            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('file',files);

            var progress = $('#progress');
            var percent = $('#percent');
            var file_location =  $('#first_valid_id_file');

            progress.removeClass('bg-danger');
            progress.addClass('bg-success');

            $.ajax({

                  url : $(this).data('url'),
                  method : 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
           
                  beforeSend: function() {
                    
                    var percentage = '0%';
                    percent.html(percentage + ' Complete');
                    progress.css("width", percentage);

                  },
                 xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            percent.html(percentComplete + '% Complete');
                            progress.css("width", percentComplete+ '%');
                        }
                   }, false);
                   return xhr;
                },
                 
                success: function(data) {

                   file_location.val(data.file_location);
                      
                },
                 error: function (xhr, ajaxOptions, thrownError) {
                

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(JSON.parse(xhr.responseText).message);
                    toastr.error(JSON.parse(xhr.responseText).errors.file[0]);
                    progress.removeClass('bg-success');
                    progress.addClass('bg-danger');
                    percent.html('Failed to upload.');


                }
              });

        });


    $('#second_valid_id').on('change', function(){

           var fd = new FormData();
            var files = $('#second_valid_id')[0].files[0];

            var imagefile = files.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
          
                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.error('Only jpeg, jpg and png Images type allowed');

              return false;
            }

            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('file',files);

            var progress = $('#scndprogress');
            var percent = $('#scndpercent');
            var file_location =  $('#second_valid_id_file');

            progress.removeClass('bg-danger');
            progress.addClass('bg-success');

            $.ajax({

                  url : $(this).data('url'),
                  method : 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
           
                  beforeSend: function() {
                    
                    var percentage = '0%';
                    percent.html(percentage + ' Complete');
                    progress.css("width", percentage);

                  },
                 xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            percent.html(percentComplete + '% Complete');
                            progress.css("width", percentComplete+ '%');
                        }
                   }, false);
                   return xhr;
                },
                 
                success: function(data) {

                   file_location.val(data.file_location);
                      
                },
                 error: function (xhr, ajaxOptions, thrownError) {
                    

                

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(JSON.parse(xhr.responseText).message);
                    toastr.error(JSON.parse(xhr.responseText).errors.file[0]);
                    progress.removeClass('bg-success');
                    progress.addClass('bg-danger');
                    percent.html('Failed to upload.');


                }
              });

        });




  });
</script>
</body>
</html>




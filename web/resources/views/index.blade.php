<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>containsLocation()</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
   html, body, #map-canvas {
    height: 100%;
    width: 100%;
    margin: 0px;
    padding: 0px
}
    </style>
  </head>
  <body>
<div id="map-canvas" style="border: 2px solid #3872ac;"></div>    <script>
   var map,
    searchArea,
    searchAreaMarker,
    searchAreaRadius = 5000, // metres
    startLat = 14.18887515,
    startLng = 121.10993041       
;

function init() {   
    var startLatLng = new google.maps.LatLng(startLat, startLng);

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: startLatLng,
        zoom: 12
    });

    searchArea = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.2,
        map: map,
        center: startLatLng,
        radius: searchAreaRadius
    });

    searchAreaMarker = new google.maps.Marker({
        position: startLatLng,
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: 'searchAreaMarker'
    });

    var randomMarkers = [
        { title: 'Marker 1', latLng: new google.maps.LatLng(14.19316597, 121.12598419) },
         { title: 'Marker 1', latLng: new google.maps.LatLng(14.18451189, 121.16718292) },
              
    ];

    for (var i = 0; i < randomMarkers.length; i++) {
        randomMarkers[i].marker = new google.maps.Marker({
            position: randomMarkers[i].latLng,
            map: map,
            title: randomMarkers[i].title
        });
    }

      for (var i = 0; i < randomMarkers.length; i++) {
            console.log('Marker: ' + randomMarkers[i].marker.title + ', position: ' + randomMarkers[i].marker.getPosition()); 

            // ---------- Here comes the error: 
            // TypeError: e is undefined
            if (google.maps.geometry.spherical.computeDistanceBetween(randomMarkers[i].marker.getPosition(), searchArea.getCenter()) <= searchArea.getRadius()) {
                alert('=> is in searchArea');
            } else {
                console.log('=> is NOT in searchArea');
            }
        }

    google.maps.event.addListener(searchAreaMarker, 'dragend', function(e) {
        startLatLng = e.latLng;

        searchArea.setOptions({
            center: startLatLng
        });

        map.panTo(searchAreaMarker.getPosition());

        // find markers in area
      
    });
}

     </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyXzfrXuKeardoOze30WW9wq6SzDT4_T4&callback=init&libraries=geometry,places">
    </script>
  </body>
</html>

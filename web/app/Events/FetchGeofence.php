<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FetchGeofence implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $lat;
    public $lng;
    public $user_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($lat, $lng, $user_id)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->user_id = $user_id;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('fetch-geofence-user-'.$this->user_id);
    }
}

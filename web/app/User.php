<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Model\CrimeReport;

class User extends Authenticatable
{
    use Notifiable, \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','middle_name', 'gender','city','province','home_address','barangay','role', 'mobile','is_user_valid', 'email', 'password','first_valid_id', 'second_valid_id', 'profile_picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function crimeReport() {

        return $this->hasMany(CrimeReport::class);

    }

    public function userCrimeReport() {

        return $this->hasMany(UserCrimeReport::class);

    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserCrimeReport extends Model
{
    public function crimeReport() {

    	return $this->belongsTo(CrimeReport::class);

    }

    public function user() {

    	return $this->belongsTo(User::class); 

    }

    public function pendingTo() {

    	return User::findOrFail($this->pending_to);

    }

}

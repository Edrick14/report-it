<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model
{
    
	public function crimeReports() {

		return $this->hasMany(CrimeReport::class);

	}

}

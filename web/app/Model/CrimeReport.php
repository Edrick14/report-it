<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\User;

class CrimeReport extends Model
{
    
	public function crime() {

		return $this->belongsTo(Crime::class);

	}

	public function barangay() {

		return $this->belongsTo(Barangay::class);

	}

	public function user() {

		return $this->belongsTo(User::class);

	}


	public function approvedBy() {

		return $this->belongsTo(User::class, 'approved_by','id');

	}

	public function userCrimeReport() {

		return $this->hasMany(UserCrimeReport::class);

	}



	public function approveBy() {

		return User::findOrFail($this->approve_by);

	}

}

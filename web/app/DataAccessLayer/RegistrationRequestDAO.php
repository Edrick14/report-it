<?php

namespace App\DataAccessLayer;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class RegistrationRequestDAO
{ 

    public function accept($id) {

        $user = User::findOrFail($id);
        $user->is_user_valid = true;
        $user->save();

        return $user;

    }

    public function decline($id) {

        $user = User::findOrFail($id);

    }

	 /*Ajax Server Processing*/
    public function collection() {

        return User::where('is_user_valid', false)->where('role', 'civilian');

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {


       return $this->getCollection()->where('first_name', 'like', "%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ->orWhere('last_name','like',"%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ->orWhere('mobile','like',"%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ->orWhere('home_address','like',"%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ->orWhere('barangay','like',"%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ->orWhere('email','like',"%{$search}%")->where('is_user_valid', false)->where('role', 'civilian')
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->getCollection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'created_at', 
                '1' => 'first_name', 
                '2' => 'home_address',  
                '3' => 'barangay',  
                '4' => 'mobile', 
                '5' => 'email', 
                '6' => 'email', 
                '7' => 'email', 
                '8' =>  'email', 
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['date'] = Carbon::parse($model->created_at)->toFormattedDateString();
                    $nestedData['name'] = $model->first_name .' '. $model->last_name;
                    $nestedData['home_address'] = $model->home_address;
                    $nestedData['barangay'] = $model->barangay;
                    $nestedData['mobile'] = $model->mobile;
                    $nestedData['email'] = $model->email ;
                    $nestedData['first_valid_id'] = '<center><a href="'.url('').$model->first_valid_id.'" target="_blank"><img src="'.url('').$model->first_valid_id.'" alt="" border=3 height=50 width=50></img></a></center>' ;
                    $nestedData['second_valid_id'] ='<center><a href="'.url('').$model->second_valid_id.'" target="_blank"><img src="'.url('').$model->second_valid_id.'" alt="" border=3 height=50 width=50></img></a></center>';
                    $nestedData['action'] = view('admin.manage-users.registration-requests.table.action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->getCollection();
    
        $totalData = $this->getCollection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }

}

<?php

namespace App\DataAccessLayer;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminDAO 
{


  public function save(Request $request, User $user) {

        $is_success = false;

        $validator = \Validator::make($request->all(),  [

            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required',
            'mobile' => 'required',
            'home_address' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',

        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->middle_name = $request->middle_name;
            $user->mobile = $request->mobile;
            $user->gender = $request->gender;
        
            $user->home_address = $request->home_address;
            $user->barangay = $request->barangay;
            $user->city = $request->city;
            $user->province = $request->province;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 'admin';
            $user->is_user_valid = true;
            
            $user->save();

            $is_success = true;


        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());

    }

      /*Ajax Server Processing*/
    public function collection() {

        return User::where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id);

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {
            

       return $this->getCollection()->where('first_name', 'like', "%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)
                            ->orWhere('last_name','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)
                            ->orWhere('mobile','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)
                            ->orWhere('home_address','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)
                            ->orWhere('barangay','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)
                            ->orWhere('email','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'admin')->where('id', '!=', \Auth::user()->id)     
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->getCollection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'first_name', 
                '1' => 'home_address',  
                '2' => 'barangay',  
                '3' => 'mobile', 
                '4' => 'email', 
                '5' => 'city', 
                '6' => 'province', 
                // '7' => 'province', 
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['name'] = $model->first_name .' '. $model->last_name;
                    $nestedData['home_address'] = $model->home_address;
                    $nestedData['barangay'] = $model->barangay;
                    $nestedData['mobile'] = $model->mobile;
                    $nestedData['email'] = $model->email ;
                    $nestedData['city'] = $model->city ;
                    $nestedData['province'] = $model->province ;
                    // $nestedData['action'] = view('admin.manage-users.admins.table.action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->getCollection();
    
        $totalData = $this->getCollection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }
}

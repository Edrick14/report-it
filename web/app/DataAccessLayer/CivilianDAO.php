<?php

namespace App\DataAccessLayer;

use Illuminate\Http\Request;

use App\User;

class CivilianDAO 
{
	
     /*Ajax Server Processing*/
    public function collection() {

        return User::where('is_user_valid', true)->where('role', 'civilian');

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {


       return $this->collection()->where('first_name', 'like', "%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ->orWhere('last_name','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ->orWhere('mobile','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ->orWhere('home_address','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ->orWhere('barangay','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ->orWhere('email','like',"%{$search}%")->where('is_user_valid', true)->where('role', 'civilian')
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->collection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'first_name', 
                '1' => 'home_address',  
                '2' => 'barangay',  
                '3' => 'mobile', 
                '4' => 'email', 
                // '5' =>  'email', 
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['name'] = $model->first_name .' '. $model->last_name;
                    $nestedData['home_address'] = $model->home_address;
                    $nestedData['barangay'] = $model->barangay;
                    $nestedData['mobile'] = $model->mobile;
                    $nestedData['email'] = $model->email ;
                    // $nestedData['action'] = view('admin.manage-users.civilians.table.action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->collection();
    
        $totalData = $this->collection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }
    
}


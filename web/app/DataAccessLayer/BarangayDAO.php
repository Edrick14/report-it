<?php

namespace App\DataAccessLayer;
use Illuminate\Http\Request;
use App\Model\Barangay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;


class BarangayDAO
{
    
	public function store(Request $request, Barangay $barangay) {

        $is_success = false;

        $request->request->add(['name' => $request->barangay]);

        $validator = \Validator::make($request->all(),  [

            'name' => 'required|unique:barangays',
           
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

         
            $barangay->name = $request->barangay;
            $barangay->save();

            $is_success = true;

        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());


	}

    public function edit(Request $request, Barangay $barangay) {

        $existing_name = $barangay->name;

        $barangay->name = '';
        $barangay->save();

        $is_success = false;

        $request->request->add(['name' => $request->barangay]);

        $validator = \Validator::make($request->all(),  [

            'name' => 'required|unique:barangays',
           
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $barangay->name = $existing_name    ;
            $barangay->save();

            $is_success = false;

        }else {

         
            $barangay->name = $request->barangay;
            $barangay->save();

            $is_success = true;

        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());


    }

	public function delete(Request $request) {

		$barangay = Barangay::find(Crypt::decrypt($request->encrypted_id));
		$barangay->delete();

	}

     /*Ajax Server Processing*/
    public function collection() {

        return Barangay::select('id','created_at', 'name')->orderby('name', 'asc');

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {


       return $this->getCollection()->where('name', 'like', "%{$search}%")
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->getCollection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'name', 
                '1' => 'name',  
                '2' => 'name',  
       
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['date'] = Carbon::parse($model->created_at)->toFormattedDateString();
                    $nestedData['barangay'] = $model->name;
                    $nestedData['action'] = view('admin.settings.includes.table.barangay-action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->getCollection();
    
        $totalData = $this->getCollection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }
    
}

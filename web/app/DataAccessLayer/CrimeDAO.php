<?php

namespace App\DataAccessLayer;

use Illuminate\Http\Request;
use App\Model\Crime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class CrimeDAO
{
   


	public function store(Request $request, Crime $crime) {

	
        $is_success = false;

        $request->request->add(['name' => $request->incident]);
        $request->request->add(['marker' => $request->file_location]);

        $validator = \Validator::make($request->all(),  [

            'name' => 'required|unique:crimes',
            'marker' => 'required',
           
        
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

            $crime->name = $request->name;
            $crime->marker = $request->marker;
            $crime->save();


            $is_success = true;


        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());

	}


    public function edit(Request $request, Crime $crime) {

    
        $existing_name = $crime->name;

        $crime->name = '';
        $crime->save();

        $is_success = false;

        $request->request->add(['name' => $request->incident]);
        $request->request->add(['marker' => $request->file_location]);

        $validator = \Validator::make($request->all(),  [

            'name' => 'required|unique:crimes',
            'marker' => 'required',
           
        
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $crime->name = $existing_name ;
            $crime->save();
            $is_success = false;

        }else {

            $crime->name = $request->name;
            $crime->marker = $request->marker;
            $crime->save();


            $is_success = true;


        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());

    }



	public function delete(Request $request) {

		$crime = Crime::find(Crypt::decrypt($request->encrypted_id));
		$crime->delete();

	}


    public function model() {

        return new Crime();

    }

     /*Ajax Server Processing*/
    public function collection() {

        return Crime::select('id','created_at', 'marker', 'name')->orderby('name', 'asc');

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {


       return $this->getCollection()->where('name', 'like', "%{$search}%")
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->getCollection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'name', 
                '1' => 'name',  
                '2' => 'marker',  
                '3' => 'name',  
       
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['date'] = Carbon::parse($model->created_at)->toFormattedDateString();
                    $nestedData['incident'] = $model->name;
                    $nestedData['marker'] = '<center><img src="'.url('').$model->marker.'" alt="" border=3 height=50 width=50></img></center>';
                    $nestedData['action'] = view('admin.settings.includes.table.incident-action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->getCollection();
    
        $totalData = $this->getCollection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }
    


}

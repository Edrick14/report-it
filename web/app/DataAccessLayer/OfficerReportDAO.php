<?php

namespace App\DataAccessLayer;


use Illuminate\Http\Request;
use App\Model\CrimeReport;
use App\Model\UserCrimeReport;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\User;

use App\Events\SendToOnlineOfficers;

use App\DataAccessLayer\DAL;

use App\Http\Resources\CrimeReportResource;

class OfficerReportDAO
{

	public function __construct($id) {

		$this->id = $id;

	}

       /*Ajax Server Processing*/
    public function collection() {

        return CrimeReport::select('*')->orderBy('id','desc')->where('approved_by', $this->id);

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {


       return $this->collection->where('first_name', 'like', "%{$search}%")->where('approved_by', $this->id)
                            ->orWhere('last_name','like',"%{$search}%")->where('approved_by', $this->id)
                            ->orWhere('middle_name','like',"%{$search}%")->where('approved_by', $this->id)
                            ->orWhere('mobile','like',"%{$search}%")->where('approved_by', $this->id)
                            ->orWhere('location','like',"%{$search}%")->where('approved_by', $this->id)
                            ->orWhere('status','like',"%{$search}%")->where('approved_by', $this->id)
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->collection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'created_at', 
                '1' => 'response_at',  
                '2' => 'name',  
                '3' => 'crime_id',  
                '4' => 'barangay_id',  
                // '5' => 'location', 
                '6' =>  'status', 
                '7' =>  'status', 
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                   
                    $nestedData['reported_at'] = Carbon::parse($model->created_at)->timezone('GMT+8')->toFormattedDateString() .' ' . Carbon::parse($model->created_at)->timezone('GMT+8')->format('g:i A');
                    $nestedData['response_at'] = isset($model->response_at) ? Carbon::parse($model->response_at)->timezone('GMT+8')->toFormattedDateString() .' ' . Carbon::parse($model->response_at)->timezone('GMT+8')->format('g:i A') : '';                   
                    $nestedData['name'] = $model->last_name .', '. $model->first_name .  ' '. $model->middle_name;
                    $nestedData['type_of_crime'] = $model->crime->name;
                    $nestedData['barangay'] = $model->barangay->name;
                    // $nestedData['location'] = $model->location ;
                    $nestedData['status'] = $model->status == 'pending'  ? '<span class="badge badge-warning right">Pending</span>' : '<span class="badge badge-success right">Reported</span>';
                    $nestedData['action'] = view('admin.reports.table.action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->collection;
    
        $totalData = $this->collection->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }   
}

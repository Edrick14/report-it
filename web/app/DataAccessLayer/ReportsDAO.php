<?php

namespace App\DataAccessLayer;


use Illuminate\Http\Request;
use App\Model\CrimeReport;
use App\Model\UserCrimeReport;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\User;

use App\Events\SendToOnlineOfficers;

use App\DataAccessLayer\DAL;

use App\Http\Resources\CrimeReportResource;


class ReportsDAO
{
    

    public function save(Request $request, CrimeReport $report) {



        $is_success = false;

        $validator = \Validator::make($request->all(),  [

            'first_name' => 'required',
            'last_name' => 'required',

            'reporter_home_address' => 'required',
            'reporter_barangay' => 'required',
            'reporter_city' => 'required',
            'reporter_province' => 'required',

            // 'incident_image' => 'required',

            'mobile' => 'required',
         
           
        
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

            $report->first_name = $request->first_name;
            $report->last_name = $request->last_name;
            $report->middle_name = $request->middle_name;

            $report->reporter_home_address = $request->reporter_home_address;
            $report->reporter_barangay = $request->reporter_barangay;
            $report->reporter_city = $request->reporter_city;
            $report->reporter_province = $request->reporter_province;

            $report->mobile = $request->mobile;
            $report->reporter_type = $request->reporter_type;
            $report->crime_id = $request->incident;
            $report->barangay_id = $request->barangay;
            $report->status = 'accepted';
            $report->location = $request->location;
            $report->lat = $request->lat;
            $report->lng = $request->lng;
            $report->description = $request->description;
            $report->response_at = Carbon::now();

            $report->image = $request->incident_image;


            $report->save();




            $is_success = true;


        }

        return array('report' => $report,'is_success' => $is_success, 'errors' => $validator->errors());


    }

     public function officerSave(Request $request, CrimeReport $report) {



        $is_success = false;

        $validator = \Validator::make($request->all(),  [

            'first_name' => 'required',
            'last_name' => 'required',

            'reporter_home_address' => 'required',
            'reporter_barangay' => 'required',
            'reporter_city' => 'required',
            'reporter_province' => 'required',

            // 'incident_image' => 'required',

            'mobile' => 'required',
         
        
           
        
        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

        

            $report->first_name = $request->first_name;
            $report->last_name = $request->last_name;
            $report->middle_name = $request->middle_name;

            $report->reporter_home_address = $request->reporter_home_address;
            $report->reporter_barangay = $request->reporter_barangay;
            $report->reporter_city = $request->reporter_city;
            $report->reporter_province = $request->reporter_province;

            $report->mobile = $request->mobile;
            $report->reporter_type = $request->reporter_type;
            $report->crime_id = $request->incident;
            $report->barangay_id = $request->barangay;
            $report->status = 'accepted';
            $report->approved_by = \Auth::user()->id;
            $report->location = $request->location;
            $report->lat = $request->lat;
            $report->lng = $request->lng;
            $report->description = $request->description;
            $report->response_at = Carbon::now();

            $report->image = $request->incident_image;

            $report->save();


            $is_success = true;


        }

        return array('report' => $report,'is_success' => $is_success, 'errors' => $validator->errors());


    }

     public function civilianSave(Request $request, CrimeReport $report) {



        $is_success = false;

        $validator = \Validator::make($request->all(),  [

            'first_name' => 'required',
            'last_name' => 'required',

            'reporter_home_address' => 'required',
            'reporter_barangay' => 'required',
            'reporter_city' => 'required',
            'reporter_province' => 'required',

            // 'incident_image' => 'required',

            'mobile' => 'required',
           
        
           
        
        ],['required' => 'This field is required.']);


        $dal = new DAL();
        
        $officers_online = $dal->OfficerDAO()->collection()->get()->filter->isOnline();
        $officers = $dal->OfficerDAO()->collection()->get();

        if($validator->fails()) {

            $is_success = false;

        }else {

        

            $report->first_name = $request->first_name;
            $report->last_name = $request->last_name;
            $report->middle_name = $request->middle_name;

            $report->reporter_home_address = $request->reporter_home_address;
            $report->reporter_barangay = $request->reporter_barangay;
            $report->reporter_city = $request->reporter_city;
            $report->reporter_province = $request->reporter_province;

            $report->mobile = $request->mobile;
            $report->reporter_type = $request->reporter_type;
            $report->crime_id = $request->incident;
            $report->barangay_id = $request->barangay;
            $report->status = 'pending';
            $report->location = $request->location;
            $report->lat = $request->lat;
            $report->lng = $request->lng;
            $report->description = $request->description;
            $report->user_id = \Auth::user()->id;

            $report->image = $request->incident_image;

            $report->save();


            if($officers_online->count() == 0) {

                $this->sendToOfficer($officers, $report);

                // event(new SendToOnlineOfficers($report, $officers));

            } else {

                // $this->sendToOfficer($officers_online, $report);

                event(new SendToOnlineOfficers(new CrimeReportResource($report), $officers_online));


            }






            $is_success = true;

        }

        return array('is_emergency' => $request->emergency, 'officer_online_count' => $officers_online->count(),'report' => $report,'is_success' => $is_success, 'errors' => $validator->errors());


    }

    public function delete(Request $request) {

        $crime_report = CrimeReport::find(Crypt::decrypt($request->encrypted_id));
        foreach ($crime_report->userCrimeReport as  $model) {
            $model->delete();
        }
        $crime_report->delete();

    }

    public function accept($id) {

        $crime_report =  CrimeReport::findOrFail($id);
        $crime_report->status = 'accepted';
        $crime_report->approved_by = \Auth::user()->id;
        $crime_report->response_at = Carbon::now();
        $crime_report->save();

        
        foreach (UserCrimeReport::where('crime_report_id', $id)->get() as $model) {
            $model->delete();
        };

        return $crime_report;

    }

    public function details($id) {

        return CrimeReport::find($id);

    }

    public function sendToOfficer($collection, $crime_report) {

        foreach ($collection as $key => $model) {
            
            $user_crime_report = new UserCrimeReport;
            $user_crime_report->crime_report_id = $crime_report->id;
            $user_crime_report->pending_to = $model->id;

            $user_crime_report->save();

        }
    }

    public function pendingOfficerCollection() {

        return UserCrimeReport::select('*')->orderBy('id','desc')->where('pending_to', \Auth::user()->id);

    }

     /*Ajax Server Processing*/
    public function collection() {

        return CrimeReport::select('*')->orderBy('id','desc');

    }

    public function setCollection($collection) {

        $this->collection = $collection;

    }

    public function getCollection() {

        return $this->collection;

    }

    public function collectionFilterPagination($search, $start, $limit, $order, $dir) {

        return $this->collectionFilter($search)   
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

    }

    public function collectionFilter($search) {
    
        $test = ($search == 'reported') ? 'accepted' : $search;

       return $this->collection()->where('first_name', 'like', "%{$search}%")
                            ->orWhere('middle_name','like',"%{$search}%")
                            ->orWhere('id', sprintf('%00d', $search))
                            ->orWhere('last_name','like',"%{$search}%")
                            ->orWhere('mobile','like',"%{$search}%")
                            ->orWhere('location','like',"%{$search}%")
                            ->orWhere('status','like',"%{$search}%")
                            ->orWhere('status','like',"%{$test}%")
                            ->orWhereHas('crime', function($query) use ($search)
                            {
                                $query->where('name','like',"%{$search}%");
                            })
                             ->orWhereHas('barangay', function($query) use ($search)
                            {
                                $query->where('name','like',"%{$search}%");
                            })
                            ->orWhereHas('approvedBy', function($query) use ($search)
                            {
                                $query->where('first_name','like',"%{$search}%")->orWhere('last_name','like',"%{$search}%");
                            })
                            ;

    }

     public function collectionPagination($start, $limit, $order, $dir) {

       return $this->collection()
                     ->offset($start)
                     ->limit($limit)
                     ->orderBy($order, $dir)
                     ;

    }
    

    public function columns() {

        return array(
                '0' => 'id', 
                '1' => 'created_at', 
                '2' => 'response_at',  
                '3' => 'first_name',  
                '4' => 'crime_id',  
                '5' => 'barangay_id',  
                // '5' => 'location', 
                '6' => 'approved_by', 
                '7' =>  'status', 
                '8' =>  'status', 
              
            );

    }

    public function data($collection) {
    
         $data = array();

             if(!empty($collection->get()))
             {
         
                foreach ($collection->get() as $model) {

                        $nestedData['id'] = sprintf('%06d', $model->id);
                    $nestedData['reported_at'] = Carbon::parse($model->created_at)->timezone('GMT+8')->toFormattedDateString() .' ' . Carbon::parse($model->created_at)->timezone('GMT+8')->format('g:i A');
                    $nestedData['response_at'] = isset($model->response_at) ? Carbon::parse($model->response_at)->timezone('GMT+8')->toFormattedDateString() .' ' . Carbon::parse($model->response_at)->timezone('GMT+8')->format('g:i A') : '';                   
                    $nestedData['name'] = $model->last_name .', '. $model->first_name .  ' '. $model->middle_name;
                    $nestedData['type_of_crime'] = $model->crime->name;
                    $nestedData['barangay'] = $model->barangay->name;
                    // $nestedData['location'] = $model->location ;
                    $nestedData['approved_by'] = !isset($model->approved_by) ? '' : '<label>'.User::findOrFail($model->approved_by)->first_name .' '. User::findOrFail($model->approved_by)->last_name .'</label>' ;
                    $nestedData['status'] = $model->status == 'pending'  ? '<span class="badge badge-warning right">Pending</span>' : '<span class="badge badge-success right">Reported</span>';
                    $nestedData['action'] = view('admin.reports.table.action', compact('model'))->render();
                        
                    $data[] = $nestedData;

                }

            }

        return $data;

    }

    public function output(Request $request) {


        $collection = $this->collection();
    
        $totalData =$this->collection()->count();

        $columns = $this->columns();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

         
        if(empty($request->input('search.value'))){

            $collection = $this->collectionPagination($start, $limit, $order, $dir);
            $totalFiltered = $this->collection()->count();

     
        }else{

            $search = $request->input('search.value');
            $collection = $this->collectionFilterPagination($search, $start, $limit, $order, $dir); 
            $totalFiltered =   $this->collectionFilter($search)->count();
     
        }           
          
        return array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $this->data($collection)   
        );

    }   
}

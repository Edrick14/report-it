<?php

namespace App\DataAccessLayer;


class DAL
{
    
	public function BarangayDAO() {

		return new BarangayDAO();

	}
    
	public function CrimeDAO() {

		return new CrimeDAO();

	}

	public function CivilianDAO() {

		return new CivilianDAO();

	}

	public function RegistrationRequestDAO() {

		return new RegistrationRequestDAO();

	}

	public function ReportsDAO() {

		return new ReportsDAO();

	}

	public function OfficerDAO() {

		return new OfficerDAO();

	}

	public function OfficerReportDAO($id)  {

		return new OfficerReportDAO($id);

	}

	public function AdminDAO() {

		return new AdminDAO();

	}

	public function ProfileDAO() {

		return new ProfileDAO();

	}

}

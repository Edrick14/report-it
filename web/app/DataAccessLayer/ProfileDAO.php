<?php

namespace App\DataAccessLayer;

use Illuminate\Http\Request;
use App\User;
use Auth;
class ProfileDAO 
{
    public function save(Request $request) {

  		$is_success = false;

        $validator = \Validator::make($request->all(),  [

            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required',
            'mobile' => 'required',
            'home_address' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            

        ],['required' => 'This field is required.']);

        if($validator->fails()) {

            $is_success = false;

        }else {

	    	$user = User::find(Auth::user()->id);

	    	$user->first_name = $request->first_name;
	    	$user->last_name = $request->last_name;
	    	$user->middle_name = $request->middle_name;
	    	$user->mobile = $request->mobile;
	    	$user->gender = $request->gender;
	    	$user->home_address = $request->home_address;
	    	$user->barangay = $request->barangay;
	    	$user->city = $request->city;
	    	$user->province = $request->province;

	    	$user->save();

            $is_success = true;

        }

        return array('is_success' => $is_success, 'errors' => $validator->errors());


    }

    public function removeProfilePhoto() {

    	$user = User::find(Auth::user()->id);

    	$user->profile_picture = null;

    	$user->save();

    }

}


<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Crypt;

class BarangayResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

            return [
                'data' => $this->collection->transform(function($model){
                    return [

                        'name' =>  $model->name,
                        'encrypted_id' => Crypt::encrypt($model->id),
                    ];
                }),
            ];


    }
}

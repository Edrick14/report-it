<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Crypt;

class PendingCrimeReportsResource extends ResourceCollection
{
      /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
                'data' => $this->collection->transform(function($model){
                    return [

                        'name' => $model->crimeReport->first_name .' '. $model->crimeReport->last_name,
                        'incident' => $model->crimeReport->crime->name,
                        'barangay' => $model->crimeReport->barangay->name,
                        'datetime' => \Carbon\Carbon::parse($model->crimeReport->created_at)->timezone('GMT+8')->toFormattedDateString() .' '. \Carbon\Carbon::parse($model->crimeReport->created_at)->timezone('GMT+8')->format('g:i A'),
                         'report_details_url' => route('officer.report.details', Crypt::encrypt($model->crimeReport->id)),
                        

                    ];
                }),
            ];

    }
}

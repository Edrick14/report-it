<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Crypt;

class CrimeReportsResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
                'data' => $this->collection->transform(function($model){
                    return [

                        'name' => $model->first_name .' '. $model->last_name,
                        'incident' => $model->crime->name,
                        'barangay' => $model->barangay->name,
                        'datetime' => \Carbon\Carbon::parse($model->created_at)->timezone('GMT+8')->toFormattedDateString() .' '. \Carbon\Carbon::parse($model->created_at)->timezone('GMT+8')->format('g:i A'),
                         'report_details_url' => route('officer.report.details', Crypt::encrypt($model->id)),

                    ];
                }),
            ];

    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class CrimeReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'data' => parent::toArray($request),
            'id' => $this->id,
            'image_url' => isset($this->image) ?  url('').$this->image  : asset('/img/no-photo-available.jpg'),
            'lat' => $this->lat,
            'lng' => $this->lng,
            'incident' => $this->crime->name,
            'name' => $this->first_name . ' ' .$this->last_name ,
            'report_details_url' => route('officer.report.details', Crypt::encrypt($this->id)),
            'route_url' => route('officer.report.route', Crypt::encrypt($this->id)),
            'accept_url' =>  route('officer.report.accept'),
            'encrypted_id' =>  Crypt::encrypt($this->id)


        ];
    }
}

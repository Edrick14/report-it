<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class IncidentsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

           return [
                'data' => $this->collection->transform(function($model){
                    return [

                        'name' => $model->name,
                         'marker_url' => url('') . $model->marker

                    ];
                }),
            ];

    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MapCrimeReportsResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    

        return [
                'data' => $this->collection->transform(function($model){
                    return [

                        'incident' => $model->crime->name,
                        'lat' => (float)$model->lat,
                        'lng' => (float)$model->lng,
                        'icon_url' => url('').$model->crime->marker,

                    ];
                }),
            ];

    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [

                'columns' => [

                    ['label' => 'Name', 'field' => 'name', 'sort' => 'asc'] ,
                    ['label' => 'Email', 'field' => 'email', 'sort' => 'asc']   ,
                    ['label' => 'Role', 'field' => 'role', 'sort' => 'asc'] 

                 ],

                'rows' =>  parent::toArray($request)
         

        ];


    }
}

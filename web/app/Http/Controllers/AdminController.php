<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;

use App\User;

class AdminController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    public function index()
    {
        return view('admin.manage-users.admins.index');
    }

    public function registerAdmin(Request $request) {

        if(request()->ajax()) {

            $dao = $this->dal->AdminDAO();

            return response()->json($dao->save($request, new User()));

        }
       
    }
    public function serverSideAdmins(Request $request) {

        $dao = $this->dal->AdminDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;
use App\Events\LoadTotalOfficer;

class OfficerController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_officers = $this->dal->OfficerDAO()->collection()->count();
        return view('admin.manage-users.police-officers.index', compact('total_officers'));
    }

     public function profile($encrypted_id) {

        $user = User::find(Crypt::decrypt($encrypted_id));
        return view('admin.manage-users.police-officers.profile.index', compact('user'));

    }

    public function registerOfficer(Request $request) {

        if(request()->ajax()) {

            $dao = $this->dal->OfficerDAO();

            return response()->json($dao->save($request, new User()));

        }
       
    }

    public function eventLoadTotalOfficer() {

        event(new LoadTotalOfficer());

        return response()->json(['is_success' => true]);

    }

    public function serverSideOfficerReports(Request $request) {

        $dao = $this->dal->OfficerReportDAO(Crypt::decrypt($request->encrypted_id));

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));


    }

    public function serverSideOfficers(Request $request) {


        $dao = $this->dal->OfficerDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));


    }

}

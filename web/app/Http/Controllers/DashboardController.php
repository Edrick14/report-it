<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;

class DashboardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(\Auth::user()->role == 'civilian') {
            return redirect()->route('civilian.dashboard');
        }

        if(\Auth::user()->role == 'officer') {
            return redirect()->route('officer.dashboard');
        }

        $total_requests = $this->dal->RegistrationRequestDAO()->collection()->count();
        $total_crime_reports = $this->dal->ReportsDAO()->collection()->count();
        $total_civilians = $this->dal->CivilianDAO()->collection()->count();
        $total_officers = $this->dal->OfficerDAO()->collection()->count();

        $dal = $this->dal;

        return view('admin.dashboard.index', compact('dal', 'total_requests', 'total_crime_reports', 'total_civilians','total_officers'));
    }

    public function statistics($encrypted_id) {


        $barangay = $this->dal->BarangayDAO()->collection()->where('id', Crypt::decrypt($encrypted_id))->first();


        $crimes = $this->dal->CrimeDAO()->collection()->get();
        $crimes_name = array();
        $crimes_total = array();
        foreach ($crimes as $key => $model) {
            array_push($crimes_name, $model->name);
            array_push($crimes_total, $model->crimeReports->where('status','accepted')->where('barangay_id', $barangay->id)->count());

        }



         $datasets = [(object) [ 'label'=> 'Crimes',
                  'fillColor'           => 'rgba(210, 214, 222, 1)',
                  'strokeColor'         => 'rgba(210, 214, 222, 1)',
                  'pointColor'          => 'rgba(210, 214, 222, 1)',
                  'pointStrokeColor'    => '#c1c7d1',
                  'pointHighlightFill'  => '#fff',
                  'borderWidth'  => 1,
                  'pointHighlightStroke'=> 'rgba(220,220,220,1)',
                  'data'                => $crimes_total]];

           $data = (object)
            [

                'labels' => $crimes_name,
                 'datasets' =>  $datasets

            ];
       


        return view('admin.dashboard.statistics.index', compact('barangay', 'data'));


    }


    public function civilianIndex() {

        if(\Auth::user()->role == 'admin') {
            return redirect()->route('admin.dashboard');
        }

        if(\Auth::user()->role == 'officer') {
            return redirect()->route('officer.dashboard');
        }

        $dal = $this->dal;
        return view('civilian.dashboard.index', compact('dal'));

    }

     public function officerIndex() {


        if(\Auth::user()->role == 'admin') {
            return redirect()->route('admin.dashboard');
        }

        if(\Auth::user()->role == 'civilian') {
            return redirect()->route('civilian.dashboard');
        }

        $dal = $this->dal;
        return view('officer.dashboard.index', compact('dal'));

    }

    public function officerStatistics($encrypted_id) {

        $barangay = $this->dal->BarangayDAO()->collection()->where('id', Crypt::decrypt($encrypted_id))->first();


        $crimes = $this->dal->CrimeDAO()->collection()->get();
        $crimes_name = array();
        $crimes_total = array();
        foreach ($crimes as $key => $model) {
            array_push($crimes_name, $model->name);
            array_push($crimes_total, $model->crimeReports->where('status','accepted')->where('barangay_id', $barangay->id)->count());

        }



         $datasets = [(object) [ 'label'=> 'Crimes',
                  'fillColor'           => 'rgba(210, 214, 222, 1)',
                  'strokeColor'         => 'rgba(210, 214, 222, 1)',
                  'pointColor'          => 'rgba(210, 214, 222, 1)',
                  'pointStrokeColor'    => '#c1c7d1',
                  'pointHighlightFill'  => '#fff',
                  'borderWidth'  => 1,
                  'pointHighlightStroke'=> 'rgba(220,220,220,1)',
                  'data'                => $crimes_total]];

           $data = (object)
            [

                'labels' => $crimes_name,
                 'datasets' =>  $datasets

            ];
       


        return view('officer.dashboard.statistics.index', compact('barangay', 'data'));

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataAccessLayer\DAL;

class MapController extends Controller
{
    
    public function __construct() {
        
        $this->middleware('auth');
		$this->dal = new DAL;

	}

	public function officerIndex() {

		return view('officer.map.index');

	}	


}

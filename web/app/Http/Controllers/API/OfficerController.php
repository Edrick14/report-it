<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;

class OfficerController extends Controller
{

	public function __construct()
    {
        $this->dal = new DAL;
    }

    public function totalOfficers() {

        return response()->json(['total_officers' => $this->dal->OfficerDAO()->collection()->count() ]);

    }
}

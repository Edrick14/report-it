<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;

class RegistrationRequestController extends Controller
{
   	public function __construct()
    {
        $this->dal = new DAL;
    }
    
    public function totalRegistrationRequests() {

        return response()->json(['total_registration_requests' => $this->dal->RegistrationRequestDAO()->collection()->count() ]);

    }
}

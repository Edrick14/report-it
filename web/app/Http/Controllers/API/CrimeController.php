<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;
use App\Http\Resources\IncidentsCollection;
use App\Http\Resources\MapCrimeReportsResource;
use App\Http\Resources\BarangayResource;
use Carbon\Carbon;


class CrimeController extends Controller
{
   	public function __construct()
    {
        $this->dal = new DAL;
    }

    public function crimeReports(Request $request) {


    	$to = Carbon::parse($request->date_to)->toDateString();	
        $from = Carbon::parse($request->date_from)->toDateString();




    	$crime_reports = $this->dal->ReportsDAO()->collection()->where('status', 'accepted');

        if($request->encrypted_id != '0') {

            $barangay = Crypt::decrypt($request->encrypted_id);

            $crime_reports = $crime_reports->where('barangay_id', $barangay);

        }

      
        $crime_reports =  $crime_reports->whereBetween('response_at', [$from, $to]);

          $_bool = true;
        if($request->search_text != '') {

            $incidents = $this->dal->CrimeDAO()->collection()->where('name', 'like', '%' . $request->search_text . '%')->get();


            foreach ($incidents as $key => $model) {


                if($_bool) {

                     $crime_reports = $crime_reports->where('crime_id', $model->id);
                     $_bool = false;

                } else {

                     $crime_reports = $crime_reports->orWhere('crime_id', $model->id);

                }



            }


        }


        $crime_reports =  $crime_reports->get();

    	return response()->json(new MapCrimeReportsResource($crime_reports));

    }

    public function crimeBarangays(Request $request) {

        $barangays = $this->dal->BarangayDAO()->collection()->get();

        return response()->json(new BarangayResource($barangays));


    }

    public function incidents() {

        $incidents = $this->dal->CrimeDAO()->collection()->get();

        return response()->json(new IncidentsCollection($incidents));

    }

    public function todayCrimeReports() {

        $crime_reports = $this->dal->ReportsDAO()->collection()->whereDate('created_at', Carbon::now()->timezone('GMT+8'))->get();

        return response()->json(['total_crime_reports' => $crime_reports->count() ]);


    }

}

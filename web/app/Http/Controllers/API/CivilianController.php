<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;

class CivilianController extends Controller
{
    public function __construct()
    {
        $this->dal = new DAL;
    }

      
    public function totalCivilians() {

        return response()->json(['total_civilians' => $this->dal->CivilianDAO()->collection()->count() ]);

    }
}

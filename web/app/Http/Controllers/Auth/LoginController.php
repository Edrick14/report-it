<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // if(\Auth::check()) {

        //     if(\Auth::user()->role == 'admin') {

        //         $this->redirectTo =  '/admin/dashboard';

        //     } else if(\Auth::user()->role == 'officer') {

        //         $this->redirectTo =  '/officer/dashboard';

        //     } else {

        //         $this->redirectTo =  '/civilian/dashboard';

        //     }
        // } 

        $this->middleware('guest')->except('logout');
    }


    public function civilianLogin(){

        if(Auth::check()) {

            return redirect()->route('civilian.dashboard');

        }

        return view('auth.civilian.login');

    }

    public function officerLogin() {


        if(Auth::check()) {

            return redirect()->route('officer.dashboard');

        }

        return view('auth.officer.login');

    }

    public function adminLogin() {


        if(Auth::check()) {

            return redirect()->route('admin.dashboard');

        }
        return view('auth.admin.login');

    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {

        // $redirect = '/';
        // if(\Request::segment(1) == 'admin') {
        //     $redirect = route('admin.login');
        // }

        // if(\Request::segment(1) == 'civilian') {
        //     $redirect = route('civilian.login');
        // }

        // if(\Request::segment(1) == 'officer') {
        //     $redirect = route('officer.login');
        // }

        return view('auth.civilian.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        $redirect = redirect('/');
        if(auth()->user()->role == 'admin') {
            $redirect = redirect()->route('admin.login');
        }

        if(auth()->user()->role == 'civilian') {
            $redirect = redirect()->route('civilian.login');
         }

        if(auth()->user()->role == 'officer') {
            $redirect = redirect()->route('officer.login');
         }

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: $redirect;
    }

    public function redirectTo()  {

        $redirect = '/';
        if(auth()->user()->role == 'admin') {
            $redirect = '/admin/dashboard';
        }

        if(auth()->user()->role == 'civilian') {
            $redirect = '/civilian/dashboard';
        }

        if(auth()->user()->role == 'officer') {
            $redirect = '/officer/dashboard';
        }

        return $redirect;

    }
}

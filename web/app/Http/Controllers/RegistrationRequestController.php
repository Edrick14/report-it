<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataAccessLayer\DAL;
use Illuminate\Support\Facades\Crypt;
use App\Events\LoadTotalRegistrationRequest;

class RegistrationRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    public function acceptCivilian(Request $request) {

        $dao = $this->dal->RegistrationRequestDAO();

        $dao->accept(Crypt::decrypt($request->encrypted_id));
 
        return response()->json(['is_successs' => true]);

    }

    public function declineCivilian(Request $request) {

        $dao = $this->dal->RegistrationRequestDAO();

        $dao->accept(Crypt::decline($request->encrypted_id));
 
        return response()->json(['is_successs' => true]);

    }

    public function index()
    {
        $total_requests = $this->dal->RegistrationRequestDAO()->collection()->count();
        return view('admin.manage-users.registration-requests.index', compact('total_requests'));
    }

    public function eventLoadTotalRegistrationRequest() {

        event(new LoadTotalRegistrationRequest());

        return response()->json(['is_success' => true]);

    }


    public function serverSideRequests(Request $request) {

        $dao = $this->dal->RegistrationRequestDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));


    }

}

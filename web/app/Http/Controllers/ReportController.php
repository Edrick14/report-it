<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Resources\CrimeReportsResource;
use App\Http\Resources\MapCrimeReportsResource;
use App\Http\Resources\PendingCrimeReportsResource;
use App\DataAccessLayer\DAL;
use App\Model\CrimeReport;
use App\Events\TrackLatestCrime;
use App\Events\LoadTotalCrimeReport;
use App\Events\FetchGeofence;
use PDF;
use Carbon\Carbon;
use App\User;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    public function index()
    {
        $dal = $this->dal;
        return view('admin.reports.index',compact('dal'));
    }


    public function generateReport(Request $request){

        set_time_limit(0);

        $dal = $this->dal;

        $encrypted_id = $request->barangay;

        $date = $request->date;

        $report_name = 'INCIDENT_REPORT_'. Carbon::now()->timezone('GMT+8')->format('dmYHisA').'.pdf';

        $pdf = PDF::loadView('admin.reports.includes.report-data', compact('dal', 'encrypted_id', 'date'));
        return $pdf->download($report_name);

    }

    public function showReport($encrypted_id) {


        $dao = $this->dal->ReportsDAO();

        $crime_report = $dao->details(Crypt::decrypt($encrypted_id));

        return view('admin.reports.includes.show-crime-report', compact('crime_report', 'encrypted_id'));

    }

    public function civilianIndex() {

        return view('civilian.reports.index');

    }

    public function civilianCreateReportIndex() {

        $dal = $this->dal;
        return view('civilian.reports.includes.create.index', compact('dal'));

    } 

    public function officerIndex() {

       
        return view('officer.reports.index');

    }

    public function officerPendingIndex() {

        return view('officer.reports.pending.index');

    }

    public function officerAcceptedIndex() {

        $dal = $this->dal;

        return view('officer.reports.accepted.index', compact('dal'));

    }

    public function crimeReports() {

        $crime_reports = $this->dal->ReportsDAO()->collection()->where('status', 'accepted')->get();
        
        return new CrimeReportsResource($crime_reports);

    }

    public function pendingOfficerCrimeReports() {

        $crime_reports = $this->dal->ReportsDAO()->pendingOfficerCollection()->get();

        return new PendingCrimeReportsResource($crime_reports);


    }

    public function addReport(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $report = $dao->save($request, new CrimeReport());

        
        if($report['is_success']) {
             $data = [

                'incident' => $report['report']->crime->name,
                'lat' => (float)$report['report']->lat,
                'lng' => (float)$report['report']->lng,
                'icon_url' => url('').$report['report']->crime->marker,

            ];

            $this->eventTrackLatestCrime($data);
        }

        return response()->json($report);

    }

    public function setReportToOfficer(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $officer = User::where('id', \Auth::user()->id)->get();
        $report = CrimeReport::find($request->crime_report_id);

        $dao->sendToOfficer($officer, $report);

        return response()->json(['is_success' => true]);

    }

    public function fetchGeofence(Request $request) {

        event(new FetchGeofence($request->lat, $request->lng, $request->user_id));

    }

    public function officerAddReport(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $report = $dao->officerSave($request, new CrimeReport());

        if($report['is_success']) {
             $data = [

                'incident' => $report['report']->crime->name,
                'lat' => (float)$report['report']->lat,
                'lng' => (float)$report['report']->lng,
                'icon_url' => url('').$report['report']->crime->marker,

            ];

            $this->eventTrackLatestCrime($data);
        }
      
        return response()->json($report);

    }


    public function civilianAddReport(request $request) {

        $dao = $this->dal->ReportsDAO();

        $report = $dao->civilianSave($request, new CrimeReport());

        return response()->json($report);

    }
    

    public function deleteReport(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $report = $dao->delete($request);

        return response()->json(['is_success' => true]);

    }

    public function officerAcceptReport(Request $request){

        $dao = $this->dal->ReportsDAO();

        $is_accepted = CrimeReport::where('id',Crypt::decrypt($request->encrypted_id))->where('status', 'accepted')->first();

        $is_success = false;

        if(!$is_accepted) {

            $crime_report = $dao->accept(Crypt::decrypt($request->encrypted_id));

            $data = [

                'incident' => $crime_report->crime->name,
                'lat' => (float)$crime_report->lat,
                'lng' => (float)$crime_report->lng,
                'icon_url' => url('').$crime_report->crime->marker,

            ];

            $this->eventTrackLatestCrime($data);

            $is_success = true;


        }
        

    
        return response()->json(['is_success' => $is_success]);

    }


    public function eventLoadTotalCrimeReport() {

        event(new LoadTotalCrimeReport());

        return response()->json(['is_success' => true]);


    }

    public function eventTrackLatestCrime($crime_report) {

        event(new TrackLatestCrime($crime_report));

        return response()->json(['is_success' => true]);

    }



    public function officerReportDetails($encrypted_id) {

        $dao = $this->dal->ReportsDAO();

        $crime_report = $dao->details(Crypt::decrypt($encrypted_id));

        return view('officer.reports.includes.details.index', compact('crime_report', 'encrypted_id') );

    }

    public function serverSideReports(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));

    }

    public function serverSideAcceptedReports(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $dao->setCollection($dao->collection()->where('user_id', Crypt::decrypt($request->encrypted_id))->where('status', 'accepted'));

        return response()->json($dao->output($request));

    }

    public function serverSidePendingReports(Request $request) {

        $dao = $this->dal->ReportsDAO();

        $dao->setCollection($dao->collection()->where('user_id', Crypt::decrypt($request->encrypted_id))->where('status', 'pending'));

        return response()->json($dao->output($request));

    }


    public function loadAddReport() {

        $dal = $this->dal;

        return view('admin.reports.includes.add-crime-report-form', compact('dal'));

    }

    public function loadEditReport($encrypted_id) {

        return view('admin.reports.includes.edit-crime-report-form');

    }

    public function incidentRoute($encrypted_id) {


        $report  = CrimeReport::find(Crypt::decrypt($encrypted_id));

 
        return view('admin.reports.includes.route', compact('report', 'encrypted_id'));

    }



}

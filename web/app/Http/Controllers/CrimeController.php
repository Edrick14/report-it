<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Crime;
use App\DataAccessLayer\DAL;
use Illuminate\Support\Facades\Crypt;


class CrimeController extends Controller
{
    
	public function __construct() {
		
        $this->middleware('auth');
		$this->dal = new DAL;

	}

	public function addCrime(Request $request) {

		$dao = $this->dal->CrimeDAO();

		$crime = $dao->store($request, new Crime());

		return response()->json($crime);

	}

	public function editCrime(Request $request) {

		$dao = $this->dal->CrimeDAO();

		$crime = $dao->edit($request, Crime::findOrFail(Crypt::decrypt($request->encrypted_id)));

		return response()->json($crime);

	}


	public function deleteCrime(Request $request) {

		$dao = $this->dal->CrimeDAO();

		$crime = $dao->delete($request);

		return response()->json(['data' => $crime]);

	}

    public function serverSideIncidents(Request $request) {

        $dao = $this->dal->CrimeDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));

    }
}

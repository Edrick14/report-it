<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataAccessLayer\DAL;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.profile.index');
    }

    public function civilianIndex() {

        return view('civilian.profile.index');

    }

    public function officerIndex() {

        return view('officer.profile.index');

    }

    public function removeProfilePhoto(Request $request) {

        if(request()->ajax()) {

            $dao = $this->dal->ProfileDAO();

            $dao->removeProfilePhoto();

            $default_photo = auth()->user()->gender == 'male' ? asset('/img/no-photo-male.png') : asset('/img/no-photo-female.png');

            return response()->json(['default_photo' => $default_photo]);

        }

    }

    public function updateProfle(Request $request) {

        if(request()->ajax()) {

            $dao = $this->dal->ProfileDAO();

            return response()->json($dao->save($request));

        }
    }

}

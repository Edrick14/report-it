<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DataAccessLayer\DAL;
use Illuminate\Support\Facades\Crypt;

use App\Model\Barangay;

class BarangayController extends Controller
{

	public function __construct() {
        
        $this->middleware('auth');
		$this->dal = new DAL;

	}

	public function addBarangay(Request $request) {

		$dao = $this->dal->BarangayDAO();

		$barangay = $dao->store($request, new Barangay());

		return response()->json($barangay);

	}

	public function editBarangay(Request $request) {

		$dao = $this->dal->BarangayDAO();

		$barangay = $dao->edit($request, Barangay::findOrFail(Crypt::decrypt($request->encrypted_id)));

		return response()->json($barangay);

	}


	public function deleteBarangay(Request $request) {

		$dao = $this->dal->BarangayDAO();

		$barangay = $dao->delete($request);

		return response()->json(['data' => $barangay]);

	}

    public function serverSideBarangay(Request $request) {

        $dao = $this->dal->BarangayDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{

   	public function fileUploadPost(Request $request)
   	{
        $request->validate([
            'file' => 'required',
		]);
 
        $fileName = time().'.'.request()->file->getClientOriginalExtension();
 
        request()->file->move(public_path('img/markers'), $fileName);
 
        return response()->json(['is_success'=> true, 'file_url' => url('/img/markers').'/'.$fileName, 'file_location' => '/img/markers/'.$fileName]);
    }

    public function uploadIncident(Request $request)
    {

  //   	ini_set('post_max_size', '64M');
		// ini_set('upload_max_filesize', '64M');

        $request->validate([
            'file' => 'required',
		]);
 
        $fileName = time().'.'.request()->file->getClientOriginalExtension();
 
        request()->file->move(public_path('img/incidents'), $fileName);
 
        return response()->json(['is_success'=> true, 'file_url' => url('/img/incidents').'/'.$fileName, 'file_location' => '/img/incidents/'.$fileName]);
    }


 	 public function uploadValidID(Request $request)
    {

  		// ini_set('post_max_size', '64M');
		// ini_set('upload_max_filesize', '64M');

        $request->validate([
            'file' => 'required',
		]);
 
        $fileName = time().'.'.request()->file->getClientOriginalExtension();
 
        request()->file->move(public_path('img/valid_id'), $fileName);
 
        return response()->json(['is_success'=> true, 'file_url' => url('/img/valid_id').'/'.$fileName, 'file_location' => '/img/valid_id/'.$fileName]);
    }

    public function uploadProfilePicture(Request $request) {

        $data = $request->image;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);

        $profile_picture= "/img/profile/" .time().'.png';

        $path = public_path() . $profile_picture;

        $user = User::find(\Auth::user()->id);
        $user->profile_picture = $profile_picture;
        $user->save();

        file_put_contents($path, $data);

        return response()->json(['image_url' => url('') .  $user->profile_picture]);

    }

}

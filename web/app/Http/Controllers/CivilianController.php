<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\DataAccessLayer\DAL;
use App\User;
use App\Events\LoadTotalCivilian;

class CivilianController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
        $this->dal = new DAL;
    }


    public function index()
    {
        $total_civilians = $this->dal->CivilianDAO()->collection()->count();
        return view('admin.manage-users.civilians.index', compact('total_civilians'));
    }

    public function profile($encrypted_id) {

        $user = User::find(Crypt::decrypt($encrypted_id));
        return view('admin.manage-users.civilians.profile.index', compact('user'));

    }

     public function eventLoadTotalCivilian() {

        event(new LoadTotalCivilian());

        return response()->json(['is_success' => true]);

    }


    public function serverSideCivilians(Request $request) {

        $dao = $this->dal->CivilianDAO();

        $dao->setCollection($dao->collection());

        return response()->json($dao->output($request));

    }


}

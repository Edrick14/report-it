<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {

            $redirect = '/';
            if(\Request::segment(1) == 'admin') {
                $redirect = route('admin.login');
            }

            if(\Request::segment(1) == 'civilian') {
                $redirect = route('civilian.login');
            }

            if(\Request::segment(1) == 'officer') {
                $redirect = route('officer.login');
            }

            return $redirect;
        }
    }
}

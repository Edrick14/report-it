$(function () {

    $('#barangay-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_barangay_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "date"},
          {"data": "barangay"},
          {"data": "action"},
      
          
      ],


    });
    


    var _flag_barangay =false;

    $('#frm-add-barangay').on('submit', function(event){
    	event.preventDefault();

  


    	var url = $(this).attr('action');
    	var method = $(this).attr('method');

      var barangay = $('#barangay_id');
    	var name_feedback = $('#name_feedback');

    	$('#add-barangay-sbmt').html(' <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>')
      $('#add-barangay-sbmt').prop('disabled', true);

      addBarangay(url, method ,  barangay, name_feedback);

    });


    $('#frm-edit-barangay').on('submit', function(event){
      event.preventDefault();

      var url = $(this).attr('action');
      var method = $(this).attr('method');

      var barangay = $('#edit_barangay');
      var name_feedback = $('#edit_name_feedback');
      var encrypted_id = $('#encrypted_id');

      $('#edit-barangay-sbmt').html(' <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>')
      $('#edit-barangay-sbmt').prop('disabled', true);

      editBarangay(url, method ,  barangay, name_feedback, encrypted_id.val());

    });



   $('#barangay-tbl').on('click', '.edit-barangay-btn[data-action]' , function(event){

      var encrypted_id = $(this).data('encrypted-id');
      var name = $(this).data('name');

      $('#encrypted_id').val(encrypted_id);
      $('#edit_barangay').val(name);

      $('#modal-edit-barangay').modal('show');

   });

   function addBarangay(url, method, barangay, name_feedback) {

      if(_flag_barangay) {
          return;
      }

     var _flag_barangay = true;


      $.ajax({

        url : url,
        method : method,
        data: { 

          _token : $('meta[name="csrf-token"]').attr('content'),
          barangay : barangay.val()
        },
        dataType: "json",
        success : function(data){
            

           if(JSON.stringify(data.errors).includes('name')) {

               barangay.addClass('is-invalid'); name_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

               barangay.removeClass('is-invalid'); name_feedback.html('');

            }

            if(data.is_success) {      


                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.success('Barangay Added Successfully.');
                barangay.val('');

                $('#barangay-tbl').DataTable().ajax.reload();

            } 

            $('#add-barangay-sbmt').html('Add');
            $('#add-barangay-sbmt').prop('disabled', false);

            _flag_barangay = false;
 
        }
      });


   }



   function editBarangay(url, method, barangay, name_feedback , encrypted_id) {

      if(_flag_barangay) {
          return;
      }

     var _flag_barangay = true;


      $.ajax({

        url : url,
        method : method,
        data: { 

          _token : $('meta[name="csrf-token"]').attr('content'),
          barangay : barangay.val(),
          encrypted_id : encrypted_id

        },
        dataType: "json",
        success : function(data){
            

           if(JSON.stringify(data.errors).includes('name')) {

               barangay.addClass('is-invalid'); name_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

               barangay.removeClass('is-invalid'); name_feedback.html('');

            }

            if(data.is_success) {      


                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.success('Barangay Updated Successfully.');

                $('#barangay-tbl').DataTable().ajax.reload();

            } 

            $('#edit-barangay-sbmt').html('Save');
            $('#edit-barangay-sbmt').prop('disabled', false);

            _flag_barangay = false;
 
        }
      });


   }


   $('#barangay-tbl').on('click', '.remove-barangay-btn[data-action]' , function(event){
        event.preventDefault();
        
        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

         swal({
           title: "Are you sure?",
           text: "Once deleted, you will not be able to recover it.",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {

              $.ajax({

                url : url,
                method : 'POST',
                data: { 

                  _token : $('meta[name="csrf-token"]').attr('content'),
                  encrypted_id : encrypted_id

                },
                dataType: "json",
                success : function(data){
                    


                    $('#barangay-tbl').DataTable().ajax.reload();

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.success('Barangay Deleted Successfully.');


                }
              });

           } else {

           }
         });

     
      });

  });

 	

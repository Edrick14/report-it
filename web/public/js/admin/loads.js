	function loadTotalCivilian() {

 		$.ajax({

	          url :  load_total_civilians_url
,
	          method : 'GET',
	          data: { 

	            _token : $('meta[name="csrf-token"]').attr('content'),
	       
	          },
	          dataType: "json",
	          success : function(data){ }

        });

	}

	function loadTotalOfficer() {

 		$.ajax({

	          url : load_total_officers_url,
	          method : 'GET',
	          data: { 

	            _token : $('meta[name="csrf-token"]').attr('content'),
	       
	          },
	          dataType: "json",
	          success : function(data){ }

        });

	}

	function loadTotalRegistrationRequest() {

		 $.ajax({

	          url : load_total_requests_url,
	          method : 'GET',
	          data: { 

	            _token : $('meta[name="csrf-token"]').attr('content'),
	       
	          },
	          dataType: "json",
	          success : function(data){ }

        });
		
	}

	function loadTotalCrimeReports() {

		 $.ajax({

	          url : load_total_crime_reports_url,
	          method : 'GET',
	          data: { 

	            _token : $('meta[name="csrf-token"]').attr('content'),
	       
	          },
	          dataType: "json",
	          success : function(data){ }

        });
		
	}

	// function trackLatestCrime() {

	// 	$.ajax({

	//           url : track_latest_crime_url,
	//           method : 'GET',
	//           data: { 

	//             _token : $('meta[name="csrf-token"]').attr('content'),
	       
	//           },
	//           dataType: "json",
	//           success : function(data){ }

 //        });

	// }
 // 
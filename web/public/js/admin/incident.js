
$(function () {

    $('#incident-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_incident_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "date"},
          {"data": "incident"},
          {"data": "marker"},
          {"data": "action"},
      
          
      ],


    });


    $('#marker').on('change', function(){

           var fd = new FormData();
            var files = $('#marker')[0].files[0];

            var imagefile = files.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
          
                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.error('Only jpeg, jpg and png Images type allowed');

              return false;
            }



            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('file',files);

            var progress = $('#progress');
            var percent = $('#percent');
            var marker =  $('#img-marker');
            var file_location =  $('#file_location');

            progress.removeClass('bg-danger');
            progress.addClass('bg-success');

            $.ajax({

                  url : $(this).data('url'),
                  method : 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
           
                  beforeSend: function() {
                    
                    var percentage = '0%';
                    percent.html(percentage + ' Complete');
                    progress.css("width", percentage);
                    marker.show();

                  },
                 xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            percent.html(percentComplete + '% Complete');
                            progress.css("width", percentComplete+ '%');
                        }
                   }, false);
                   return xhr;
                },
                 
                success: function(data) {

                   marker.attr('src', data.file_url);
                   file_location.val(data.file_location);
                      
                },
                 error: function (xhr, ajaxOptions, thrownError) {
                    
                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(JSON.parse(xhr.responseText).message);
                    toastr.error(JSON.parse(xhr.responseText).errors.file[0]);
                    progress.removeClass('bg-success');
                    progress.addClass('bg-danger');
                    percent.html('Failed to upload.');


                }
              });

        });




    var _flag_incident = false;

    $('#frm-add-incident').on('submit', function(event){
    	event.preventDefault();

      if(_flag_incident) {
            return;
      }

     var _flag_incident = true;


    	var url = $(this).attr('action');
    	var method = $(this).attr('method');

    	var incident = $('#incident_id');
      var incident_feedback = $('#incident_feedback');

      var file_location = $('#file_location');
      var marker_feedback = $('#marker_feedback');

      var progress = $('#progress');
      var percent = $('#percent');
      var marker = $('#img-marker');

    	$('#add-incident-sbmt').html(' <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
      $('#add-incident-sbmt').prop('disabled', true);

    	$.ajax({

  			url : url,
  			method : method,
        data: { 

	      	_token : $('meta[name="csrf-token"]').attr('content'),
          incident : incident.val(),
	      	file_location : file_location.val()

        },
  			dataType: "json",
  			success : function(data){
      			


           if(JSON.stringify(data.errors).includes('name')) {

               incident.addClass('is-invalid'); incident_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

               incident.removeClass('is-invalid'); incident_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('marker')) {

               marker_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

              marker_feedback.html('');

            }


      



            if(data.is_success) {      


                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.success('Incident Added Successfully.');

                incident.val('');
                file_location.val('');
                
                percent.html('');
                progress.css("width", '0%');
                marker.hide();

                $('#incident-tbl').DataTable().ajax.reload();


            } 

            $('#add-incident-sbmt').html('Add');
            $('#add-incident-sbmt').prop('disabled', false);

            _flag_incident = false;

  			}
  		});


    });



    $('#frm-edit-incident').on('submit', function(event){
      event.preventDefault();

      if(_flag_incident) {
            return;
      }

     var _flag_incident = true;


      var url = $(this).attr('action');
      var method = $(this).attr('method');

      var incident = $('#edit_incident_id');
      var incident_feedback = $('#edit_incident_feedback');

      var file_location = $('#edit_file_location');
      var marker_feedback = $('#edit_marker_feedback');

      var progress = $('#edit_progress');
      var percent = $('#edit_percent');
      var marker = $('#edit_img-marker');

      var encrypted_id = $('#iencrypted_id');


      $('#edit-incident-sbmt').html(' <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
      $('#edit-incident-sbmt').prop('disabled', true);

      $.ajax({

        url : url,
        method : method,
        data: { 

          _token : $('meta[name="csrf-token"]').attr('content'),
          incident : incident.val(),
          file_location : file_location.val(),
          encrypted_id: encrypted_id.val()

        },
        dataType: "json",
        success : function(data){
            


           if(JSON.stringify(data.errors).includes('name')) {

               incident.addClass('is-invalid'); incident_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

               incident.removeClass('is-invalid'); incident_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('marker')) {

               marker_feedback.html('<strong>'+data.errors.name+'</strong>');

            } else {

              marker_feedback.html('');

            }


      



            if(data.is_success) {      


                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.success('Incident Update Successfully.');
                
                percent.html('');
                progress.css("width", '0%');

                $('#incident-tbl').DataTable().ajax.reload();


            } 

            $('#edit-incident-sbmt').html('Save');
            $('#edit-incident-sbmt').prop('disabled', false);

            _flag_incident = false;

        }
      });


    }); 


   $('#incident-tbl').on('click', '.remove-incident-btn[data-action]' , function(event){
        event.preventDefault();
        
        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

         swal({
           title: "Are you sure?",
           text: "Once deleted, you will not be able to recover it.",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {

              $.ajax({

                url : url,
                method : 'POST',
                data: { 

                  _token : $('meta[name="csrf-token"]').attr('content'),
                  encrypted_id : encrypted_id

                },
                dataType: "json",
                success : function(data){
                    
                      $('#incident-tbl').DataTable().ajax.reload();

                      toastr.options.progressBar = true;
                      toastr.options.positionClass = "toast-bottom-left";
                      toastr.success('Incident Deleted Successfully.');

                }
              });

           } else {

           }
         });

     
      });


   $('#incident-tbl').on('click', '.edit-incident-btn[data-action]' , function(event){

      var encrypted_id = $(this).data('encrypted-id');
      var name = $(this).data('name');
      var marker = $(this).data('marker');
      var file_url = $(this).data('file-url');

      $('#iencrypted_id').val(encrypted_id);
      $('#edit_incident_id').val(name);


      $('#edit_file_location').val(marker);
      $('#edit_img-marker').attr('src',file_url);



      $('#modal-edit-incident').modal('show');

   });


 $('#edit_marker').on('change', function(){

           var fd = new FormData();
            var files = $('#edit_marker')[0].files[0];

            var imagefile = files.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
          
                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.error('Only jpeg, jpg and png Images type allowed');

              return false;
            }



            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('file',files);

            var progress = $('#edit_progress');
            var percent = $('#edit_percent');
            var marker =  $('#edit_img-marker');
            var file_location =  $('#edit_file_location');

            progress.removeClass('bg-danger');
            progress.addClass('bg-success');

            $.ajax({

                  url : $(this).data('url'),
                  method : 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
           
                  beforeSend: function() {
                    
                    var percentage = '0%';
                    percent.html(percentage + ' Complete');
                    progress.css("width", percentage);
                    marker.show();

                  },
                 xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            percent.html(percentComplete + '% Complete');
                            progress.css("width", percentComplete+ '%');
                        }
                   }, false);
                   return xhr;
                },
                 
                success: function(data) {

                   marker.attr('src', data.file_url);
                   file_location.val(data.file_location);
                      
                },
                 error: function (xhr, ajaxOptions, thrownError) {
                    
                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(JSON.parse(xhr.responseText).message);
                    toastr.error(JSON.parse(xhr.responseText).errors.file[0]);
                    progress.removeClass('bg-success');
                    progress.addClass('bg-danger');
                    percent.html('Failed to upload.');


                }
              });

        });

 });
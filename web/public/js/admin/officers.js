$(function () {

    $('#officers-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_officers_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "name"},
          {"data": "mobile"},
          {"data": "email"},
          {"data": "home_address"},
          {"data": "barangay"},
          {"data": "action"},
          
      ],


    });



    $('#officer-reports-tbl').DataTable({
 
      "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_officer_reports_url,
          dataType: "json",
          type: "POST",
          data: {
            _token : $('meta[name="csrf-token"]').attr('content'),
           encrypted_id: $('#officer-reports-tbl').data('encrypted-id')

          } 
      },
      "columns": [

          {"data": "reported_at"},
          {"data": "response_at"},
          {"data": "name"},
          {"data": "type_of_crime"},
          {"data": "barangay"},
          // {"data": "location"},
          {"data": "status"},
          {"data": "action"},
          
      ],
    });


    $('#register-officer-btn').on('click', function(){

      $('#register-officer-wrapper').show();
      $('#cancel-register-btn').show();
      $('#register-officer-btn').hide();

    });

    $('#cancel-register-btn').on('click', function(){

      $('#register-officer-wrapper').hide();
      $('#cancel-register-btn').hide();
      $('#register-officer-btn').show();

    });

    $('#cancel2-register-btn').on('click', function(){

      $('#register-officer-wrapper').hide();
      $('#cancel-register-btn').hide();
      $('#register-officer-btn').show();

    });


     
   
      var _flag_officer = false;

      $('#frm-register-officer').on('submit', function(event){
     
         event.preventDefault();


         if(_flag_officer) {
            return;
         }

         _flag_officer = true;

         var url = $(this).attr('action');
         var method = $(this).attr('method');


         var first_name = $('#first_name'); 
         var first_name_feedback = $('#first_name_feedback');

         var last_name = $('#last_name'); 
         var last_name_feedback = $('#last_name_feedback');

         var middle_name = $('#middle_name'); 
         var middle_name_feedback = $('#middle_name_feedback');

         var mobile =  $('#mobile');
         var mobile_feedback =  $('#mobile_feedback');

         var home_address =  $('#home_address');
         var home_address_feedback =  $('#home_address_feedback');

         var position =  $('#position');
         var position_feedback =  $('#position_feedback');

         var destination =  $('#destination');
         var destination_feedback =  $('#destination_feedback');

         var gender = $('#gender');

         var barangay =  $('#barangay');
         var barangay_feedback =  $('#barangay_feedback');

         var city =  $('#city');
         var city_feedback =  $('#city_feedback');

          var province =  $('#province');
         var province_feedback =  $('#province_feedback');

         var email =  $('#email');
         var email_feedback =  $('#email_feedback');

         var password =  $('#password');
         var password_feedback =  $('#password_feedback');

         var confirm_password =  $('#confirm_password');
         

          $('#register-officer-sbmt').html('<i class="fa fa-refresh fa-spin"></i>')
          $('#register-officer-sbmt').prop('disabled', true);

          //ajax here

         $.ajax({

          url : url,
          method : method,
          data: { 

            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : first_name.val(),
            last_name : last_name.val(),
            middle_name : middle_name.val(),
            gender : gender.val(),
            destination : destination.val(),
            position : position.val(),
            mobile : mobile.val(),
            home_address: home_address.val(),
            barangay : barangay.val(),
            city: city.val(),
            province: province.val(),
            email: email.val(),
            password: password.val(),
            password_confirmation: confirm_password.val()

          },
          dataType: "json",
          success : function(data){
            


            
            if(JSON.stringify(data.errors).includes('first_name')) {

               first_name.addClass('is-invalid'); first_name_feedback.html('<strong>'+data.errors.first_name+'</strong>');

            } else {

               first_name.removeClass('is-invalid'); first_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('last_name')) {

               last_name.addClass('is-invalid'); last_name_feedback.html('<strong>'+data.errors.last_name+'</strong>');

            } else {

               last_name.removeClass('is-invalid'); last_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('middle_name')) {

               middle_name.addClass('is-invalid'); middle_name_feedback.html('<strong>'+data.errors.middle_name+'</strong>');

            } else {

               middle_name.removeClass('is-invalid'); middle_name_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('destination')) {

               destination.addClass('is-invalid'); destination_feedback.html('<strong>'+data.errors.destination+'</strong>');

            } else {

               destination.removeClass('is-invalid'); destination_feedback.html('');

            }

              if(JSON.stringify(data.errors).includes('position')) {

               position.addClass('is-invalid'); position_feedback.html('<strong>'+data.errors.position+'</strong>');

            } else {

               position.removeClass('is-invalid'); position_feedback.html('');

            }



            if(JSON.stringify(data.errors).includes('email')) {

              email.addClass('is-invalid'); email_feedback.html('<strong>'+data.errors.email+'</strong>');

            } else {

              email.removeClass('is-invalid'); email_feedback.html('');


            }

            if(JSON.stringify(data.errors).includes('mobile')) {

              mobile.addClass('is-invalid'); mobile_feedback.html('<strong>'+data.errors.mobile+'</strong>');

            }else {

              mobile.removeClass('is-invalid'); mobile_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('barangay')) {

              barangay.addClass('is-invalid'); barangay_feedback.html('<strong>'+data.errors.barangay+'</strong>');

            } else {

              barangay.removeClass('is-invalid'); barangay_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('home_address')) {

              home_address.addClass('is-invalid'); home_address_feedback.html('<strong>'+data.errors.home_address+'</strong>');

            } else {

              home_address.removeClass('is-invalid'); home_address_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('city')) {

              city.addClass('is-invalid'); city_feedback.html('<strong>'+data.errors.city+'</strong>');

            } else {

              city.removeClass('is-invalid'); city_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('province')) {

              province.addClass('is-invalid'); province_feedback.html('<strong>'+data.errors.province+'</strong>');

            } else {

              province.removeClass('is-invalid'); province_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('password')) {

              password.addClass('is-invalid'); password_feedback.html('<strong>'+data.errors.password+'</strong>');

            } else {

              password.removeClass('is-invalid'); password_feedback.html('');

            }

           


            if(data.is_success) {

               $('#cancel-register-btn').hide();
               $('#register-officer-btn').show();  

              toastr.options.progressBar = true;
              toastr.options.positionClass = "toast-bottom-left";
              toastr.success('Officer Registered Successfully.');

              resetRegistrationForm();

              $('#register-officer-wrapper').hide();

              $('#officers-tbl').DataTable().ajax.reload();

            } 

            $('#register-officer-sbmt').html('Register');
            $('#register-officer-sbmt').prop('disabled', false);              
            _flag_officer = false;

            loadTotalOfficer();


          }

        });

      });

      function resetRegistrationForm() {

         $('#first_name').val(''); 
         $('#last_name').val(''); 
         $('#middle_name').val(''); 
         $('#mobile').val('');
         ('#destination').val('');
         ('#position').val('');
         $('#province').val('');
         $('#home_address').val('');
         $('#barangay').val('');
         $('#city').val('');
         $('#email').val('');
         $('#password').val('');
         $('#confirm_password').val('');

      }

      $('#refresh-officers-btn').on('click', function(event){
          event.preventDefault();

          $('#officers-tbl').DataTable().ajax.reload();


      });

  });

 	

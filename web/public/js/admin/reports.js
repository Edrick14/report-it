 $(function () {
    
    $('#reports-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_reports_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          } 
      },
      "columns": [

          {"data": "id"},
          {"data": "reported_at"},
          {"data": "response_at"},
          {"data": "name"},
          {"data": "type_of_crime"},
          {"data": "barangay"},
          // {"data": "location"},
          {"data": "approved_by"},
          {"data": "status"},
          {"data": "action"},
          
      ],
    });



  $('#refresh-crime-report-btn').on('click', function(event){
      event.preventDefault();

      $('#reports-tbl').DataTable().ajax.reload();


  });

$('#accepted-reports-tbl').DataTable({
 
      "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_accepted_reports_url,
          dataType: "json",
          type: "POST",
          data: {
            _token : $('meta[name="csrf-token"]').attr('content'),
            encrypted_id: $('#accepted-reports-tbl').data('encrypted-id')

          }
      },
      "columns": [

          {"data": "reported_at"},
          {"data": "response_at"},
          {"data": "name"},
          {"data": "type_of_crime"},
          {"data": "barangay"},
          // {"data": "location"},
          // {"data": "approved_by"},
          {"data": "status"},
          {"data": "action"},
          
      ],
    });


$('#pending-reports-tbl').DataTable({
 
      "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_pending_reports_url,
          dataType: "json",
          type: "POST",
          data: {
            _token : $('meta[name="csrf-token"]').attr('content'),
            encrypted_id: $('#pending-reports-tbl').data('encrypted-id')
          }
      },
      "columns": [

         {"data": "reported_at"},
          {"data": "response_at"},
          {"data": "name"},
          {"data": "type_of_crime"},
          {"data": "barangay"},
          // {"data": "location"},
          // {"data": "approved_by"},
          {"data": "status"},
          {"data": "action"},
          
      ],
    });


    $('#nav-pending').on('click', function() {

          $('#pending-reports-tbl').DataTable().ajax.reload();


    });

    $('#nav-accepted').on('click', function() {

          $('#accepted-reports-tbl').DataTable().ajax.reload();

    });





    $('#add-crime-report-btn').on('click', function(){

      $('#add-report-wrapper').show();
      $('#cancel-crime-report-btn').show();
      $('#add-crime-report-btn').hide();

   // $.ajax({

   //        url : admin_report_load_add,
   //        method : 'POST',
   //        data: { 

   //          _token : $('meta[name="csrf-token"]').attr('content'),

   //        },
   //        success : function(data){
              
   //            $('#add-report-wrapper').empty();
   //            $('#add-report-wrapper').html(data);
   //            $('#add-report-wrapper').show();
   //            // $('#cancel-crime-report-btn').show();
   //            $('#add-crime-report-btn').hide();

       

   //        }
   //      });


    });


    $('#cancel-crime-report-btn').on('click', function(){

      $('#add-report-wrapper').hide();
      $('#cancel-crime-report-btn').hide();
      $('#add-crime-report-btn').show();
        $('#edit-report-wrapper').hide();


    });


    $('#cancel2-crime-report-btn').on('click', function(){

      $('#add-report-wrapper').hide();
      $('#cancel-crime-report-btn').hide();
      $('#add-crime-report-btn').show();


    });

    $('#cancel3-crime-report-btn').on('click', function(){

      $('#edit-report-wrapper').hide();
      $('#cancel-crime-report-btn').hide();
      $('#add-crime-report-btn').show();
      

    });

    $('#frm-crime-report').bind('keydown', function(e) {
          if (e.keyCode == 13) {
              e.preventDefault();
          }
    });

    var _flag = false;

    $('#frm-crime-report').on('submit', function(event){
     
       event.preventDefault();

        if(_flag) {
          return;
       }

       _flag = true;

       var url = $(this).attr('action');
       var method = $(this).attr('method');

       var civilian = $(this).data('civilian');

          
       var first_name = $('#first_name'); 
       var first_name_feedback = $('#first_name_feedback');

       var last_name = $('#last_name'); 
       var last_name_feedback = $('#last_name_feedback');

       var middle_name = $('#middle_name'); 
       var middle_name_feedback = $('#middle_name_feedback');


       var reporter_home_address = $('#reporter_home_address'); 
       var reporter_home_address_feedback = $('#reporter_home_address_feedback');

      var reporter_barangay = $('#reporter_barangay'); 
      var reporter_barangay_feedback = $('#reporter_barangay_feedback');

        
      var reporter_city = $('#reporter_city'); 
      var reporter_city_feedback = $('#reporter_city_feedback');

      var reporter_province = $('#reporter_province'); 
      var reporter_province_feedback = $('#reporter_province_feedback');


       var mobile =  $('#mobile');
       var mobile_feedback =  $('#mobile_feedback');

       var reporter_type =  $('#reporter_type');
       var incident =  $('#incident');
       var barangay =  $('#barangay');

       var location =  $('#location');
       var location_feedback =  $('#location_feedback');

       var lat =  $('#lat');
       var lng =  $('#lng');
       var description =  $('textarea#description');


       var incident_image = $('#image_file');
       var incident_image_feedback = $('#image_file_feedback');


        $('#add-report-sbmt').html('<i class="fa fa-refresh fa-spin"></i>')
        $('#add-report-sbmt').prop('disabled', true);

        $.ajax({

          url : url,
          method : method,
          data: { 

            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : first_name.val(),
            last_name : last_name.val(),
            middle_name : middle_name.val(),
            reporter_home_address : reporter_home_address.val(),
            reporter_barangay : reporter_barangay.val(),
            reporter_city : reporter_city.val(),
            reporter_province : reporter_province.val(),
            mobile : mobile.val(),
            reporter_type : reporter_type.val(),
            incident : incident.val(),
            barangay : barangay.val(),
            description : description.val(),
            location: location.val(),
            lat: lat.val(),
            lng: lng.val(),
            incident_image : incident_image.val(),
            emergency :  $('#emergency').prop('checked')

          },
          dataType: "json",
          success : function(data){
            
                if(JSON.stringify(data.errors).includes('incident_image')) {

                   incident_image.addClass('is-invalid'); incident_image_feedback.html('<strong>'+data.errors.incident_image+'</strong>');

                } else {

                   incident_image.removeClass('is-invalid'); incident_image_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('reporter_home_address')) {

                   reporter_home_address.addClass('is-invalid'); reporter_home_address_feedback.html('<strong>'+data.errors.reporter_home_address+'</strong>');

                } else {

                   reporter_home_address.removeClass('is-invalid'); reporter_home_address_feedback.html('');

                }

               if(JSON.stringify(data.errors).includes('reporter_barangay')) {

                   reporter_barangay.addClass('is-invalid'); reporter_barangay_feedback.html('<strong>'+data.errors.reporter_barangay+'</strong>');

                } else {

                   reporter_barangay.removeClass('is-invalid'); reporter_barangay_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('reporter_city')) {

                   reporter_city.addClass('is-invalid'); reporter_city_feedback.html('<strong>'+data.errors.reporter_city+'</strong>');

                } else {

                   reporter_city.removeClass('is-invalid'); reporter_city_feedback.html('');

                }


                if(JSON.stringify(data.errors).includes('reporter_province')) {

                   reporter_province.addClass('is-invalid'); reporter_province_feedback.html('<strong>'+data.errors.reporter_province+'</strong>');

                } else {

                   reporter_province.removeClass('is-invalid'); reporter_province_feedback.html('');

                }


                if(JSON.stringify(data.errors).includes('middle_name')) {

                   middle_name.addClass('is-invalid'); middle_name_feedback.html('<strong>'+data.errors.middle_name+'</strong>');

                } else {

                   middle_name.removeClass('is-invalid'); middle_name_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('last_name')) {

                   last_name.addClass('is-invalid'); last_name_feedback.html('<strong>'+data.errors.last_name+'</strong>');

                } else {

                   last_name.removeClass('is-invalid'); last_name_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('first_name')) {

                   first_name.addClass('is-invalid'); first_name_feedback.html('<strong>'+data.errors.first_name+'</strong>');

                } else {

                   first_name.removeClass('is-invalid'); first_name_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('mobile')) {

                  mobile.addClass('is-invalid'); mobile_feedback.html('<strong>'+data.errors.mobile+'</strong>');

                }else {

                  mobile.removeClass('is-invalid'); mobile_feedback.html('');

                }

                if(JSON.stringify(data.errors).includes('location')) {

                  location.addClass('is-invalid'); location_feedback.html('<strong>'+data.errors.location+'</strong>');

                }else {

                  location.removeClass('is-invalid'); location_feedback.html('');

                }

              if(data.is_success) {

                $('#cancel-crime-report-btn').hide();
                $('#add-crime-report-btn').show();

                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-center";
                toastr.success('Crime Report Added Successfully.');

               
                if(civilian == 'yes') {

                  resetCivilianReportForm();

                } else {

                   resetReportForm();

                }

                $('#add-report-wrapper').hide();

                $('#reports-tbl').DataTable().ajax.reload();

                loadOfficerReports('accepted');

               
              } 

           
              $('#add-report-sbmt').html('Save');
              $('#add-report-sbmt').prop('disabled', false);

              _flag = false;

              loadTotalCrimeReports();

          }

        });

      });


   

    $('#reports-tbl').on('click', '.edit-report-btn[data-action]', function(event){

        event.preventDefault();

        var encrypted_id = $(this).data('encrypted-id');


        var first_name = $(this).data('first-name');
        var last_name = $(this).data('last-name');
        var middle_name = $(this).data('middle-name');
        var mobile = $(this).data('mobile');
        var reporter_type = $(this).data('reporter-type');
        var reporter_home_address = $(this).data('home-address');
        var reporter_barangay = $(this).data('barangay');
        var reporter_city = $(this).data('city');
        var reporter_province = $(this).data('province');
        var incident = $(this).data('incident-id');
        var barangay = $(this).data('barangay-id');
        var location = $(this).data('location');
        var image = $(this).data('image');
        var lat = $(this).data('lat');
        var lng = $(this).data('lng');
        var description = $(this).data('description');

        $('#edit_first_name').val(first_name);
        $('#edit_last_name').val(last_name);
        $('#edit_middle_name').val(middle_name);
        $('#edit_mobile').val(mobile);
        $("#edit_reporter_type").val(reporter_type).change();
        $("#edit_reporter_home_address").val(reporter_home_address);
        $("#edit_reporter_barangay").val(reporter_barangay);
        $("#edit_reporter_city").val(reporter_city);
        $("#edit_reporter_province").val(reporter_province);
        $("#edit_incident").val(incident).change();
        $("#edit_barangay").val(barangay).change();

        $("#edit_image_file").val(image).change();

        $("#edit_location").val(location);

        $("#edit_lat").val(lat);
        $("#edit_lng").val(lng);

        $("#description").val(description);



        $('#add-report-wrapper').hide();
        $('#cancel-crime-report-btn').show();
        $('#add-crime-report-btn').hide();
        $('#edit-report-wrapper').show();

    });

    $('#reports-tbl').on('click', '.remove-report-btn[data-action]', function(event){

        event.preventDefault();

        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

        swal({
           title: "Are you sure?",
           text: "Once deleted, you will not be able to recover it.",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {

              $.ajax({

                  url : url,
                  method : 'POST',
                  data: { 

                    _token : $('meta[name="csrf-token"]').attr('content'),
                    encrypted_id : encrypted_id

                  },
                  dataType: "json",
                  success : function(data){
                      
                        $('#reports-tbl').DataTable().ajax.reload();

                        toastr.options.progressBar = true;
                        toastr.options.positionClass = "toast-bottom-left";
                        toastr.success('Report Deleted Successfully.');

                  }
                });

           } else {

           }
         });



    });

    function resetReportForm() {


       $('#first_name').val('');
       $('#middle_name').val('');
       $('#last_name').val('');
       $('#reporter_home_address').val('');
       $('#reporter_barangay').val('');
       $('#reporter_city').val('');
       $('#reporter_province').val('');
       $('#mobile').val('');
       $('#location').val('');
       $('textarea#description').val('');
       $('#incident_image').val('');
       $('#percent').html('');
       $('#progress').css("width", '0%');

    }

    function resetCivilianReportForm() {

       $('#location').val('');
       $('textarea#description').val('');
       $('#incident_image').val('');
       $('#percent').html('');
       $('#progress').css("width", '0%');



    }








    function crimeReportTemplating(data, color) {

            
        var html = '';



        $.each(data, function(index, item){
            html += 

          '<div class="col-md-3 col-sm-6 col-12">'
            +'<div class="crime-report info-box '+color+'" data-url="'+item.report_details_url+'">'
              +'<span class="info-box-icon"><i class="fa fa-map-marker"></i></span>'

              +'<div class="info-box-content">'
                +'<span class="info-box-text">'+item.name+'</span>'
                +'<span class="info-box-number">'+item.incident+'</span>'
                +'<span class="info-box-text" style="font-size: 12px">'+item.datetime+'</span>'

                +'<div class="progress">'
                  +'<div class="progress-bar" style="width: 0%"></div>'
                +'</div>'
                +'<span class="progress-description">'
                   + '<span class="info-box-text"><i class="fa fa-map-marker">  </i> '+item.barangay+'</span>'
                +'</span>'
              +'</div>'
            +'</div>'
          +'</div>';

            
        });
        
        return html;
    }



    $('#refresh-preports-btn').on('click', function() {
        

        loadOfficerReports('pending');

    });

    $('#refresh-areports-btn').on('click', function() {

        loadOfficerReports('accepted');

    });


    loadOfficerReports('pending');
    loadOfficerReports('accepted');


     function loadOfficerReports(type) {

        if($('#reports-card').length) {

           $.ajax({

              url : (type === 'pending') ? officer_pending_reports_list_url : officer_reports_list_url,
              method : 'GET',
              data: { 

                _token : $('meta[name="csrf-token"]').attr('content'),
               
              },
              dataType: "json",
              success : function(data){


                  if(type === 'pending') {

                    if($('#pending-reports-pagination-container').length) {

                      $('#pending-reports-pagination-container').pagination({
                        dataSource: data.data,
                        pageSize: 5,
                        // showGoInput: true,
                        // showGoButton: true,
                        className : 'paginationjs-theme-blue',
                        callback: function(data, pagination) {
                          
                            if(JSON.stringify(data) =='[]') {

                                var html = 
                                '<div class="col-md-3 col-sm-6 col-12">'
                                  +'<div class="crime-report info-box" >'
                                    +'<div class="info-box-content text-center">'
                                   
                                      +'<span class="info-box-text" style="font-size: 15px; margin-top: 15px;">No data available.</span>'

                                  
                                    +'</div>'
                                  +'</div>'
                                +'</div>';
                               $('#pending-reports-container').html(html);
                               return;
                            }

                            var html = crimeReportTemplating(data, 'bg-warning');
                            $('#pending-reports-container').html(html);
                        }
                      });    

                    }  

                  } else {

                    if($('#reports-pagination-container').length) {

                       $('#reports-pagination-container').pagination({
                          dataSource: data.data,
                          pageSize: 5,
                          // showGoInput: true,
                          // showGoButton: true,
                          className : 'paginationjs-theme-blue',
                          callback: function(data, pagination) {

                              if(JSON.stringify(data) =='[]') {

                                var html = 
                                '<div class="col-md-12 col-sm-12 col-12">'
                                  +'<div class="crime-report info-box" >'
                                    +'<div class="info-box-content text-center">'
                                   
                                      +'<span class="info-box-text" style="font-size: 15px; margin-top: 15px;">No data available.</span>'

                                  
                                    +'</div>'
                                  +'</div>'
                                +'</div>';
                               $('#reports-container').html(html);
                               return;
                            }

                              var html = crimeReportTemplating(data, 'bg-success');
                              $('#reports-container').html(html);
                          }
                        });      

                      }   

                  }

                
              }

            });

        }

     }

     $('#accept-crime-report-btn').on('click', function(event){
        event.preventDefault();

        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

        swal({
           title: "Are you sure?",
           text: "",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willAccept) => {
           if (willAccept) {

               $('#accept-crime-report-btn').html('<i class="fa fa-refresh fa-spin"></i>')
               $('#accept-crime-report-btn').prop('disabled', true);


               $.ajax({

                  url : url,
                  method : 'POST',
                  data: { 

                    _token : $('meta[name="csrf-token"]').attr('content'),
                    encrypted_id : encrypted_id

                  },
                  dataType: "json",
                  success : function(data){
                      
                         if(data.is_success) {

                            swal("Great!", "Report accepted Successfully!", "success");

                          } else {

                            swal({
                                title: "Oooppss!",
                                text: "Report is already accepted!",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                              });

                          }


                        $('#accept-container').empty();

                        $('#accept-crime-report-btn').html('Accept')
                        $('#accept-crime-report-btn').prop('disabled', false);



                  }
                });

           } else {

           }
         });

     });

     $('#reports-card').on('click', '.crime-report[data-url]', function(){

        var type = $('#reports-card').data('type');

        if(type == "no") {
          
          return;

        }

        var url = $(this).data('url');

        window.location = url;

     });



    $('#incident_image').on('change', function(){

           var fd = new FormData();
            var files = $('#incident_image')[0].files[0];

            var imagefile = files.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
          
                toastr.options.progressBar = true;
                toastr.options.positionClass = "toast-bottom-left";
                toastr.error('Only jpeg, jpg and png Images type allowed');

              return false;
            }

            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('file',files);

            var progress = $('#progress');
            var percent = $('#percent');
            var file_location =  $('#image_file');

            progress.removeClass('bg-danger');
            progress.addClass('bg-success');

            $.ajax({

                  url : $(this).data('url'),
                  method : 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
           
                  beforeSend: function() {
                    
                    var percentage = '0%';
                    percent.html(percentage + ' Complete');
                    progress.css("width", percentage);

                  },
                 xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            percent.html(percentComplete + '% Complete');
                            progress.css("width", percentComplete+ '%');
                        }
                   }, false);
                   return xhr;
                },
                 
                success: function(data) {

                   file_location.val(data.file_location);
                      
                },
                 error: function (xhr, ajaxOptions, thrownError) {
                    

                  alert(JSON.stringify(thrownError));
                  alert(JSON.stringify(ajaxOptions));
                  alert(JSON.stringify(xhr));

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(JSON.parse(xhr.responseText).message);
                    toastr.error(JSON.parse(xhr.responseText).errors.file[0]);
                    progress.removeClass('bg-success');
                    progress.addClass('bg-danger');
                    percent.html('Failed to upload.');


                }
              });

        });

    $('#frm-generate-report').on('submit', function(event){

      event.preventDefault();

      var url = $(this).attr('action');

      $('#generate-sbmt').html('<i class="fa fa-refresh fa-spin"></i>');
      $('#generate-sbmt').prop('disabled', true);


      toastr.options.progressBar = true;
      toastr.options.positionClass = "toast-bottom-left";
      toastr.info('Generating your report. Please wait...');

      $.ajax({
            cache: false,
            type: 'POST',
            url: url,
            data: { 

                  _token : $('meta[name="csrf-token"]').attr('content'),
                  barangay : $('#generate_barangay').val(),
                  date: $('#generate_date').val()

            },
             //xhrFields is what did the trick to read the blob to pdf
            xhrFields: {
                responseType: 'blob'
            },
            success: function (response, status, xhr) {

                var filename = "";                   
                var disposition = xhr.getResponseHeader('Content-Disposition');

                 if (disposition) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches !== null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                } 
                var linkelem = document.createElement('a');


                  $('#generate-sbmt').html('Generate');
                  $('#generate-sbmt').prop('disabled', false);
                  
                  toastr.options.progressBar = true;
                  toastr.options.positionClass = "toast-bottom-left";
                  toastr.success('Report Generated Successfully.');


                try {
                     var blob = new Blob([response], { type: 'application/octet-stream' });                        

                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        //   IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) { 
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement("a");

                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.target = "_blank";
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }
                    }   

            
                } catch (ex) {
                    console.log(ex);

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error(ex);

                } 
            }
        });



    });


  });

 	

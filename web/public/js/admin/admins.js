

 $(function () {


    $('#admins-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_admins_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "name"},
          {"data": "mobile"},
          {"data": "email"},
          {"data": "home_address"},
          {"data": "barangay"},
          {"data": "city"},
          {"data": "province"},
          // {"data": "action"},
          
      ],


    });

    $('#refresh-admins-btn').on('click', function(){

        $('#admins-tbl').DataTable().ajax.reload();
       $('#register-admin-btn').show();


    });

    $('#register-admin-btn').on('click', function() {

      $('#register-admin-wrapper').show();
       $('#register-admin-btn').hide();

    });

    $('#cancel2-admin-btn').on('click', function(){

      $('#register-admin-wrapper').hide();
       $('#register-admin-btn').show();

    });


 
   
      var _flag_admin = false;

      $('#frm-register-admin').on('submit', function(event){
     
         event.preventDefault();


         if(_flag_admin) {
            return;
         }

         _flag_admin = true;

         var url = $(this).attr('action');
         var method = $(this).attr('method');


         var first_name = $('#first_name'); 
         var first_name_feedback = $('#first_name_feedback');

         var last_name = $('#last_name'); 
         var last_name_feedback = $('#last_name_feedback');

         var middle_name = $('#middle_name'); 
         var middle_name_feedback = $('#middle_name_feedback');

         var mobile =  $('#mobile');
         var mobile_feedback =  $('#mobile_feedback');

         var home_address =  $('#home_address');
         var home_address_feedback =  $('#home_address_feedback');

         var gender = $('#gender');

         var barangay =  $('#barangay');
         var barangay_feedback =  $('#barangay_feedback');

         var city =  $('#city');
         var city_feedback =  $('#city_feedback');

          var province =  $('#province');
         var province_feedback =  $('#province_feedback');

         var email =  $('#email');
         var email_feedback =  $('#email_feedback');

         var password =  $('#password');
         var password_feedback =  $('#password_feedback');

         var confirm_password =  $('#confirm_password');
         

          $('#register-admin-sbmt').html('<i class="fa fa-refresh fa-spin"></i>')
          $('#register-admin-sbmt').prop('disabled', true);

          //ajax here

         $.ajax({

          url : url,
          method : method,
          data: { 

            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : first_name.val(),
            last_name : last_name.val(),
            middle_name : middle_name.val(),
            gender : gender.val(),
            mobile : mobile.val(),
            home_address: home_address.val(),
            barangay : barangay.val(),
            city: city.val(),
            province: province.val(),
            email: email.val(),
            password: password.val(),
            password_confirmation: confirm_password.val()

          },
          dataType: "json",
          success : function(data){
            


            
            if(JSON.stringify(data.errors).includes('first_name')) {

               first_name.addClass('is-invalid'); first_name_feedback.html('<strong>'+data.errors.first_name+'</strong>');

            } else {

               first_name.removeClass('is-invalid'); first_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('last_name')) {

               last_name.addClass('is-invalid'); last_name_feedback.html('<strong>'+data.errors.last_name+'</strong>');

            } else {

               last_name.removeClass('is-invalid'); last_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('middle_name')) {

               middle_name.addClass('is-invalid'); middle_name_feedback.html('<strong>'+data.errors.middle_name+'</strong>');

            } else {

               middle_name.removeClass('is-invalid'); middle_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('email')) {

              email.addClass('is-invalid'); email_feedback.html('<strong>'+data.errors.email+'</strong>');

            } else {

              email.removeClass('is-invalid'); email_feedback.html('');


            }

            if(JSON.stringify(data.errors).includes('mobile')) {

              mobile.addClass('is-invalid'); mobile_feedback.html('<strong>'+data.errors.mobile+'</strong>');

            }else {

              mobile.removeClass('is-invalid'); mobile_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('barangay')) {

              barangay.addClass('is-invalid'); barangay_feedback.html('<strong>'+data.errors.barangay+'</strong>');

            } else {

              barangay.removeClass('is-invalid'); barangay_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('home_address')) {

              home_address.addClass('is-invalid'); home_address_feedback.html('<strong>'+data.errors.home_address+'</strong>');

            } else {

              home_address.removeClass('is-invalid'); home_address_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('city')) {

              city.addClass('is-invalid'); city_feedback.html('<strong>'+data.errors.city+'</strong>');

            } else {

              city.removeClass('is-invalid'); city_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('province')) {

              province.addClass('is-invalid'); province_feedback.html('<strong>'+data.errors.province+'</strong>');

            } else {

              province.removeClass('is-invalid'); province_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('password')) {

              password.addClass('is-invalid'); password_feedback.html('<strong>'+data.errors.password+'</strong>');

            } else {

              password.removeClass('is-invalid'); password_feedback.html('');

            }

           


            if(data.is_success) {

               $('#cancel-register-btn').hide();
               $('#register-admin-btn').show();  

              toastr.options.progressBar = true;
              toastr.options.positionClass = "toast-bottom-left";
              toastr.success('Admin Registered Successfully.');

              resetAdminRegistrationForm();

              $('#register-admin-wrapper').hide();

              $('#admins-tbl').DataTable().ajax.reload();

            } 

            $('#register-admin-sbmt').html('Register');
            $('#register-admin-sbmt').prop('disabled', false);              
            _flag_admin = false;

          }

        });

      });

    function resetAdminRegistrationForm(){

         $('#first_name').val(''); 
         $('#last_name').val(''); 
         $('#middle_name').val(''); 
         $('#mobile').val('');
         $('#province').val('');
         $('#home_address').val('');
         $('#barangay').val('');
         $('#city').val('');
         $('#email').val('');
         $('#password').val('');
         $('#confirm_password').val('');
    }

  });

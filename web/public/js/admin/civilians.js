

 $(function () {


    $('#users-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_user_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "name"},
          {"data": "mobile"},
          {"data": "email"},
          {"data": "home_address"},
          {"data": "barangay"},
          // {"data": "action"},
          
      ],


    });

    $('#refresh-civilians-btn').on('click', function(){

        $('#users-tbl').DataTable().ajax.reload();

    });

  });

 	



 $(function () {


    $('#requests-tbl').DataTable({
 
  	  "responsive": true,
      "processing": true,
      "serverSide": true, 

      "ajax": {
          url:  server_side_request_url,
          dataType: "json",
          type: "POST",
          data: {
          	_token : $('meta[name="csrf-token"]').attr('content')
          }
      },
      "columns": [

          {"data": "date"},
          {"data": "name"},
          {"data": "mobile"},
          {"data": "email"},
          {"data": "home_address"},
          {"data": "barangay"},
          {"data": "first_valid_id"},
          {"data": "second_valid_id"},
          {"data": "action"},
          
      ],


    });

    $('#refresh-requests-btn').on('click', function(){

        $('#requests-tbl').DataTable().ajax.reload();

    });

    /*Accept Button*/
    $('#requests-tbl').on('click', '.accept-civilian-btn[data-action]', function(event){

        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

      swal({
           title: "Are you sure?",
           text: "",
           icon: "info",
           buttons: true,
           dangerMode: false,
         })
         .then((willAccept) => {
           if (willAccept) {

                $.ajax({

                  url : url,
                  method : 'POST',
                  data: { 

                    _token : $('meta[name="csrf-token"]').attr('content'),
                    encrypted_id : encrypted_id

                  },
                  dataType: "json",
                  success : function(data){
                      
                        $('#requests-tbl').DataTable().ajax.reload();

                        toastr.options.progressBar = true;
                        toastr.options.positionClass = "toast-bottom-left";
                        toastr.success('Request Accepted Successfully.');

                        loadTotalCivilian();
                        loadTotalRegistrationRequest();

                  }
                });
 
           } else {

           }
         });

    });

    /*Decline Button*/
    $('#requests-tbl').on('click', '.decline-civilian-btn[data-action]', function(event){
      
        var encrypted_id = $(this).data('encrypted-id');
        var url = $(this).data('action');

        swal({
           title: "Are you sure?",
           text: "",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
 
           } else {

           }
         });

 
    });



  });

 	

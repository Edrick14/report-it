$(function(){

    upload();

    function upload() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    // $('.upload-demo').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result

                    }).then(function(){
                        console.log('jQuery bind complete');
                      $uploadCrop.croppie('setZoom', 0);

                    });
                 


                }
                
                reader.readAsDataURL(input.files[0]);
            }
            else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        function updateProfilePhoto() {

            $.ajax({

                url: remove_profile_picture_url,
                type: "POST",
                data: {
                    _token : $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {

                    $('.profile-photo').attr('src', data.default_photo);

                    toastr.options.progressBar = true;
                    toastr.options.positionClass = "toast-bottom-left";
                    toastr.error('Profile Picture removed successfully.');

                }
            });

        }

        var bwidth;

        $(window).resize(function(){
           cwidth = $(window).width();

           if( cwidth <= 480 && cwidth >= 320) {
             bwidth = cwidth - 40;
           } else {
             bwidth = 185;
           }
        });

        $uploadCrop = $('#upload-canvas').croppie({
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: { width: 275, height: 275 },
            showZoomer: true,
            enableResize: false,
            enableOrientation: true,
            mouseWheelZoom: 'ctrl'
        });

        $uploadCrop.croppie('bind', {

            url: user_profile_picture_url

        }).then(function(){
          
          $uploadCrop.croppie('setZoom', 0);

        });



        $('#upload-profile').on('change', function () { readFile(this); });

        $('.delete-photo').on('click', function(ev){

            ev.preventDefault();

            updateProfilePhoto();

        });

        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
            
                $.ajax({

                    url: upload_profile_picture_url,
                    type: "POST",
                    data: {
                        "image":resp,
                        _token : $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (data) {

                        $('.profile-photo').attr('src', data.image_url);

                        toastr.options.progressBar = true;
                        toastr.options.positionClass = "toast-bottom-left";
                        toastr.success('Profile Picture updated successfully.');

                    }
                });


            });
        });
    }

      var _update_flag = false;
    $('#frm-update-profile').on('submit',function(event) {

        event.preventDefault();

         if(_update_flag) {
            return;
         }

         _update_flag = true;

         var url = $(this).attr('action');
         var method = $(this).attr('method');


         var first_name = $('#first_name'); 
         var first_name_feedback = $('#first_name_feedback');

         var last_name = $('#last_name'); 
         var last_name_feedback = $('#last_name_feedback');

         var middle_name = $('#middle_name'); 
         var middle_name_feedback = $('#middle_name_feedback');

         var mobile =  $('#mobile');
         var mobile_feedback =  $('#mobile_feedback');

         var home_address =  $('#home_address');
         var home_address_feedback =  $('#home_address_feedback');

  

         var gender = $('#gender');

         var barangay =  $('#barangay');
         var barangay_feedback =  $('#barangay_feedback');

         var city =  $('#city');
         var city_feedback =  $('#city_feedback');

          var province =  $('#province');
         var province_feedback =  $('#province_feedback');


          $('#save-profile-sbmt').html('<i class="fa fa-refresh fa-spin"></i>')
          $('#save-profile-sbmt').prop('disabled', true);

          //ajax here

         $.ajax({

          url : url,
          method : method,
          data: { 

            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : first_name.val(),
            last_name : last_name.val(),
            middle_name : middle_name.val(),
            gender : gender.val(),
            mobile : mobile.val(),
            home_address: home_address.val(),
            barangay : barangay.val(),
            city: city.val(),
            province: province.val()
          

          },
          dataType: "json",
          success : function(data){
            


            
            if(JSON.stringify(data.errors).includes('first_name')) {

               first_name.addClass('is-invalid'); first_name_feedback.html('<strong>'+data.errors.first_name+'</strong>');

            } else {

               first_name.removeClass('is-invalid'); first_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('last_name')) {

               last_name.addClass('is-invalid'); last_name_feedback.html('<strong>'+data.errors.last_name+'</strong>');

            } else {

               last_name.removeClass('is-invalid'); last_name_feedback.html('');

            }


            if(JSON.stringify(data.errors).includes('middle_name')) {

               middle_name.addClass('is-invalid'); middle_name_feedback.html('<strong>'+data.errors.middle_name+'</strong>');

            } else {

               middle_name.removeClass('is-invalid'); middle_name_feedback.html('');

            }

         

            if(JSON.stringify(data.errors).includes('mobile')) {

              mobile.addClass('is-invalid'); mobile_feedback.html('<strong>'+data.errors.mobile+'</strong>');

            }else {

              mobile.removeClass('is-invalid'); mobile_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('barangay')) {

              barangay.addClass('is-invalid'); barangay_feedback.html('<strong>'+data.errors.barangay+'</strong>');

            } else {

              barangay.removeClass('is-invalid'); barangay_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('home_address')) {

              home_address.addClass('is-invalid'); home_address_feedback.html('<strong>'+data.errors.home_address+'</strong>');

            } else {

              home_address.removeClass('is-invalid'); home_address_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('city')) {

              city.addClass('is-invalid'); city_feedback.html('<strong>'+data.errors.city+'</strong>');

            } else {

              city.removeClass('is-invalid'); city_feedback.html('');

            }

            if(JSON.stringify(data.errors).includes('province')) {

              province.addClass('is-invalid'); province_feedback.html('<strong>'+data.errors.province+'</strong>');

            } else {

              province.removeClass('is-invalid'); province_feedback.html('');

            }

       
            if(data.is_success) {


              $('#user_full_name').html( first_name.val() + ' ' + last_name.val());

              toastr.options.progressBar = true;
              toastr.options.positionClass = "toast-bottom-left";
              toastr.success('Profile Updated Successfully.');

              $('#save-profile-sbmt').html('Save');
              $('#save-profile-sbmt').prop('disabled', false);   

              _update_flag = false;


            } 

          }

        });

    });

});
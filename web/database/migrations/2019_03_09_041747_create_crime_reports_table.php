<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrimeReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crime_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('crime_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('mobile')->nullable();
            $table->string('reporter_type')->nullable();

            $table->string('reporter_home_address')->nullable();
            $table->string('reporter_barangay')->nullable();
            $table->string('reporter_city')->nullable();
            $table->string('reporter_province')->nullable();

            $table->string('barangay_id')->nullable();
            $table->string('location')->nullable();
            $table->string('lng')->nullable();
            $table->string('lat')->nullable();
            $table->string('status')->nullable();
            $table->string('description')->nullable();
            $table->string('image')->nullable();
            $table->integer('approved_by')->nullable();
            $table->timestamp('response_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crime_reports');
    }
}

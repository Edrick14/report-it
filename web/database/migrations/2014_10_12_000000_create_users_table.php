<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {


            $table->bigIncrements('id');

            $table->string('first_name');

            $table->string('last_name');

            $table->string('middle_name')->nullable();


            $table->string('email')->unique();

            $table->string('mobile')->nullable();

            $table->string('home_address')->nullable();

            $table->string('barangay')->nullable();

            $table->string('city')->nullable();

            $table->string('province')->nullable();
            
            $table->string('role')->nullable();

            $table->string('first_valid_id')->nullable();

            $table->string('second_valid_id')->nullable();

            $table->boolean('is_user_valid')->default(0);

            $table->string('gender')->nullable();

            $table->string('officer_destination')->nullable();

            $table->string('officer_position')->nullable();

            $table->string('profile_picture')->nullable();

            $table->timestamp('email_verified_at')->nullable();

            $table->string('password');
            
            $table->rememberToken();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

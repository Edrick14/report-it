package com.lits.reportit.report_it;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;


public class SplashActivity extends  Activity {


    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean agreed = sharedPreferences.getBoolean("agreed",false);
        if (!agreed) {
            new AlertDialog.Builder(this)
                    .setTitle("License agreement")
                    .setPositiveButton("Agree", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("agreed", true);
                            editor.commit();

                            handler=new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            },3000);

                        }
                    })
                    .setCancelable(false)
                    .setMessage("This application should only be used for safety purposes only and the ease of communication with the police station in Calamba, Laguna. This is not intended for illegal use or for any inappropriate actions. Failing to comply with the terms of condition would not be considered to be a valid report. In case of misuse, you can call (911) to report any inappropriate use of the application.")
                    .show();
        } else {

            handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            },3000);

        }





    }


}
